

module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define(
        "User",
        {
            id: { 
                type: Sequelize.INTEGER,
                field: 'id',
                primaryKey: true, 
                autoIncrement: true 
            },
            name: {
                type: Sequelize.STRING,
                field: 'name'
            },
        },
        {
            tableName: 'user',
            underscored: true,
            timestamps: false
        }
    )

    return User
};