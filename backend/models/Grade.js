module.exports = (sequelize, Sequelize) => {
    const Grade = sequelize.define("Grade", {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        name: { type: Sequelize.STRING, allowNull: false },
        sixth: { type: Sequelize.BOOLEAN, allowNull: false }
    }, { tableName: 'grade', underscored: true});
    return Grade;
};
