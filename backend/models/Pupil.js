const Grade = require('../models').Grade;
const Teacher = require('../models').Teacher;


module.exports = (sequelize, Sequelize) => {
    const Pupil = sequelize.define(
        "Pupil",
        {
            id: { 
                type: Sequelize.INTEGER, 
                primaryKey: true, 
                autoIncrement: true 
            },
            firstName: {
                type: Sequelize.STRING,
                field: "first_name",
                allowNull: false
            },
            lastName: {
                type: Sequelize.STRING,
                field: "last_name",
                allowNull: false
            },
            mainTeacherId: {
                type: Sequelize.INTEGER,
                field: "main_teacher_id",
                references: {
                    model: Teacher,
                    key: "id"
                }
            },
            gradeId: {
                type: Sequelize.INTEGER,
                field: "grade_id",
                references: {
                    model: Grade,
                    key: "id"
                }
            }
        },
        {
            tableName: 'pupils',
            underscored: true,
            timestamps: false
        }
    );

    return Pupil;
};
