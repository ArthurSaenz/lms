const Department = require('../models').Department;


module.exports = (sequelize, Sequelize) => {
    const Teacher = sequelize.define("Teacher", {
        id: { 
            type: Sequelize.INTEGER, 
            primaryKey: true, 
            autoIncrement: true 
        },
        firstName: {
            type: Sequelize.STRING,
            field: "first_name",
            allowNull: false
        },
        lastName: {
            type: Sequelize.STRING,
            field: "last_name",
            allowNull: false
        },
        departmentId: {
            type: Sequelize.INTEGER,
            field: "department_id",
            // references: {
            //     model: Department,
            //     key: "id"
            // }
        }
    }, { 
        tableName: 'teachers', 
        underscored: true,
        timestamps: false
    });

    return Teacher;
};
