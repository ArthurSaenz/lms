

module.exports = (sequelize, Sequelize) => {
    const Tool = sequelize.define(
        "Tool",
        {   
            id: { 
                type: Sequelize.INTEGER,
                field: 'id',
                primaryKey: true, 
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING,
                field: 'name'
            }
        },
        {
            tableName: 'tool',
            underscored: true,
            timestamps: false
        }
    )

    return Tool
};