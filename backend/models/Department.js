const Teacher = require('../models').Teacher;


module.exports = (sequelize, Sequelize) => {
    const Department = sequelize.define("Department", {
        id: { 
            type: Sequelize.INTEGER, 
            primaryKey: true, 
            autoIncrement: true 
        },
        name: { 
            type: Sequelize.STRING, 
            field: "name", 
            allowNull: false 
        },
        headTeacher: {
            type: Sequelize.INTEGER,
            unique: true,
            field: "head_teacher",
            // references: {
            //     model: Teacher,
            //     key: "id"
            // }
        }
    }, {
        tableName: 'departments',
        underscored: true,
        timestamps: false
    });
    return Department;
};
