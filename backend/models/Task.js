

module.exports = (sequelize, Sequelize) => {
    const Task = sequelize.define(
        "Task",
        {
            id: { 
                type: Sequelize.INTEGER,
                field: 'id',
                primaryKey: true, 
                autoIncrement: true 
            },
            name: {
                type: Sequelize.STRING,
                field: 'name'
            },
            userId: {
                type: Sequelize.INTEGER,
                field: 'user_id'
            }
        },
        {
            tableName: 'task',
            underscored: true,
            timestamps: false
        }
    )

    return Task
};