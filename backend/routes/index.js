const express = require('express');
const pupils = require('./pupils');
const teachers = require('./teachers');
const departments = require('./departments');

const routes = express.Router();

routes.use('/pupils', pupils);
routes.use('/teachers', teachers);
routes.use('/departments', departments);

module.exports = routes;
