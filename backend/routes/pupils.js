const express = require('express');
const Pupils = require('../controllers/pupils');


const router = express.Router();

router.get('/', Pupils.getPupils);
router.get('/', Pupils.getPupilsByIds);
router.get('/:pupilId', Pupils.getPupilsById);
router.post('/', Pupils.createPupil);
router.put('/:pupilId', Pupils.updatePupil);
router.delete('/:pupilId', Pupils.deletePupil);
router.delete('/', Pupils.deletePupilMultiple);

module.exports = router;