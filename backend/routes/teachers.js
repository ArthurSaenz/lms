const express = require('express');
const Teacher = require('../controllers/teachers');


const router = express.Router();

router.get('/', Teacher.getTeachers);
router.get('/:teacherId', Teacher.getTeachersById);
router.post('/', Teacher.getTeachersById);
router.put('/:teacherId', Teacher.updateTeacher);
router.delete('/', Teacher.deleteTeacher);

module.exports = router;