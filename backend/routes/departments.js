const express = require('express');
const Departments = require('../controllers/departments');


const router = express.Router();

router.get('/', Departments.getDepartments);
router.get('/:departmentId', Departments.getDepartmentsById);
router.post('/', Departments.createDepartment);
router.put('/:departmentId', Departments.updateDepartment);
router.delete('/:departmentId', Departments.deleteDepartment);


module.exports = router;