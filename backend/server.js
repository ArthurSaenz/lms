const express = require('express');
const routes = require('./routes/index');
const bodyParser = require('body-parser');
const db = require('./models/index');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/', routes);



db.sequelize
    .authenticate()    
    .then(() => {
        console.log("Connection has been established successfully.");
    })
    .catch(err => {
        console.error("Unable to connect to the database:", err);
    });
    

app.listen(3001, () => console.log("Server started"));
