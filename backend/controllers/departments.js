const Teacher = require('../models').Teacher;
const Department = require('../models').Department;


// Teacher.hasOne(Department, { foreignKey: 'headTeacher' });
// Department.belongsTo(Teacher, {  foreignKey: 'headTeacher'});


const getDepartments = (req, res) => {
    return Department
        .findAll()
        // .findAll({include: [Teacher]})
        .then(departments => res.status(200).send(departments))
        .catch(error => res.status(400).send(error))
};


const getDepartmentsById = (req, res) => {
    const modelId = Number(req.params.departmentId);
    return Department
        .findById(modelId)
        .then( department => {
            if (!department) {
                return res.status(404).send({
                    message: 'Department Not Found'
                });
            }
            return res.status(200).send(department)
        })
        .catch(error => res.status(400).send(error))
};


const updateDepartment = (req, res) => {
    const modelId= Number(req.params.teacherID);
    return Department
        .findById(modelId)
        .then(department => {
            if (!department) {
                return res.status(404).send({
                    message: "Department Not Found"
                });
            }
            return department
                .update({
                    name: req.body.name || department.name,
                    headTeacher: req.body.headTeacher || teacher.headTeacher
                })
                .then(() => res.sendStatus(200))
                .catch(error => res.status(400).send(error))
        })
    .catch(error => res.status(400).send(error))
};


const createDepartment = (req, res) => {
    return Department
        .create({
            name: req.body.name,
            headTeacher: req.body.headTeacher
        })
        .then(department => {
            res.status(201).send({id: department.get({plain: true}).id})
        })
        .catch(error => res.status(400).send(error));
};


const deleteDepartment = (req, res) => {
    const modelId= Number(req.params.departmentID);
    return Department
        .findById(modelId)
        .then(department => {
            if (!department) {
                return res.status(400).send({
                    message: "Department Not Found"
                })
            }
            return department
                .destroy()
                .then(() => res.status(204).send())
                .catch(error => res.status(400).send(error));
        })
        .catch(error => res.status(400).send(error));
};

module.exports = {
    getDepartments,
    getDepartmentsById,
    updateDepartment,
    createDepartment,
    deleteDepartment
}