const Task = require('../models').Task;
// const Tool = require('../models').Tool;
const User = require('../models').User;


Task.belongsTo(User, { foreignKey: 'userId' }); // Task.findAll({include: [User]})
User.hasMany(Task, {foreignKey: 'userId'}); // User.findAll({include: [Task]})


const getTestAll = (req, res) => {
    return User
        .findAll({ include: [ Task ] })
        .then(items => res.status(200).send(items))
        .catch(error => res.status(400).send(error))
};

const getTestById = (req, res) => {
    const modelId = Number(req.params.teacherId);
    return Task
        .findById(modelId)
        .then( item => {
            if (!item) {
                return res.status(404).send({
                    message: 'Teacher Not Found'
                });
            }
            return res.status(200).send(item)
        })
        .catch(error => res.status(400).send(error))
};