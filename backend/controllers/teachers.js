const Teacher = require('../models').Teacher;
const Department = require('../models').Department;
const Pupil = require('../models').Pupil;

Teacher.belongsTo(Department, {foreignKey: 'departmentId'});
Department.hasMany(Teacher, {foreignKey: 'departmentId'});



const getTeachers = (req, res) => {
    return Teacher.findAll({
            include: [
                {
                    model: Pupil,
                    attributes: ["id"]
                    // as: 'pupils',
                }
            ]
        })
        .then(teachers => res.status(200).send(teachers))
        .catch(error => res.status(400).send(error));
};


const getTeachersById = (req, res) => {
    const modelId = Number(req.params.teacherId);
    return Teacher
        .findById(modelId)
        .then( teacher => {
            if (!teacher) {
                return res.status(404).send({
                    message: 'Teacher Not Found'
                });
            }
            return res.status(200).send(teacher)
        })
        .catch(error => res.status(400).send(error))
};

const getTeacherPupilsById = (req, res) => {
    const modelId = Number(req.params.teacherId);
    return 
}


const updateTeacher = (req, res) => {
    const modelId= Number(req.params.teacherID);
    return Pupil
        .findById(modelId)
        .then(teacher => {
            if (!teacher) {
                return res.status(404).send({
                    message: "Teacher Not Found"
                });
            }
            return teacher
                .update({
                    firstName: req.body.firstName || teacher.firstName,
                    lastName: req.body.lastName || teacher.lastName,
                    department: req.body.department || teacher.department
                })
                .then(() => res.sendStatus(200))
                .catch(error => res.status(400).send(error))
        })
    .catch(error => res.status(400).send(error))
};


const createTeacher = (req, res) => {
    return Teacher
        .create({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            department: req.body.department
        })
        .then(teacher => {
            res.status(201).send({id: teacher.get({plain: true}).id})
        })
        .catch(error => res.status(400).send(error));
};


const deleteTeacher = (req, res) => {
    const modelId= Number(req.params.teacherID);
    return Teacher
        .findById(modelId)
        .then(teacher => {
            if (!teacher) {
                return res.status(400).send({
                    message: "Teacher Not Found"
                })
            }
            return teacher
                .destroy()
                .then(() => res.status(204).send())
                .catch(error => res.status(400).send(error));
        })
        .catch(error => res.status(400).send(error));
};

module.exports = {
    getTeachers,
    getTeachersById,
    updateTeacher,
    createTeacher,
    deleteTeacher
}

