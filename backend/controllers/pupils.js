const Pupil = require('../models').Pupil;
const Teacher = require('../models').Teacher;
const Department = require('../models').Department;


Pupil.belongsTo(Teacher, {foreignKey: 'mainTeacherId'}); // pupils have 'Teacher'
Teacher.hasMany(Pupil, {foreignKey: 'mainTeacherId'});


const getPupils = (req, res, next) => {
    if (req.query.ids !== undefined ) { return next() };
    return Pupil
        // .findAll({ include: [{ all: true, nested: true }]}) or other variant
        .findAll({
            include: [
                {
                    model: Teacher,
                    include: [
                        { model: Department }
                    ]
                }
            ]
        })
        .then(pupils => res.status(200).send(pupils))
        .catch(error => res.status(400).send(error));
};

const getPupilsByIds = (req, res) => {
    console.log('Params array ', req.query.ids);
    return Pupil
        .findAll({ where: { id: req.query.ids }})
        .then( pupils => res.status(200).send(pupils))
        .catch(error => res.status(400).send(error));
};


const getPupilsById = (req, res) => {
    const modelId = Number(req.params.pupilId) // or req.params.id * 1;
    return Pupil
        .findById(modelId)
        .then(pupil => {
            if (!pupil) {
                return res.status(404).send({
                    message: 'Pupil Not Found',
                });
            }
            return res.status(200).send(pupil);
        })
        .catch(error => res.status(400).send(error))
};


const updatePupil = (req, res) => {
    const modelId = Number(req.params.pupilId)
    return Pupil
        .findById(modelId)
        .then(pupil => {
            if (!pupil) {
                return res.status(404).send({
                    message: 'Pupil Not Found'
                });
            }
            return pupil
                .update({
                    firstName: req.body.firstName || pupil.firstName,
                    lastName: req.body.lastName || pupil.lastName,
                    mainTeacher: req.body.mainTeacher || pupil.mainTeacher,
                    grade: req.body.grade || pupil.grade                
                })
                .then(()=> res.sendStatus(200))
                .catch(error => res.status(400).send(error));  
            })
        .catch(error => res.status(400).send(error))
}


const createPupil = (req, res) => {
    return Pupil
        .create({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            mainTeacher: req.body.mainTeacher,
            grade: req.body.grade
        })
        .then(pupil => {
            res.status(201).send({id: pupil.get({plain: true}).id})
        })
        .catch(error => res.status(400).send(error));
}


const deletePupil = (req, res) => {
    const modelId = Number(req.params.pupilId)
    return Pupil
        .findById(modelId)
        .then(pupil => {
            if (!pupil) {
                return res.status(400).send({
                    message: "Pupil Not Found"
                });
            }
            return pupil
                .destroy()
                .then(() => res.status(204).send())
                .catch(error => res.status(400).send(error));
        })
        .catch(error => res.status(400).send(error));
};


const deletePupilMultiple = ( req, res ) => {
    console.log('Delete ', req.query.ids);
    res.status(200).send()
}

module.exports = {
    getPupils,
    getPupilsByIds,
    getPupilsById,
    updatePupil,
    createPupil,
    deletePupil,
    deletePupilMultiple
};