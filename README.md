# Learning Management System SPA.

That’s a software-based platform that facilitates the management, delivery, and
measurement of an organization's processes of musical schools.


Create with Create React App and demonstrate my skills and experience in the development of the SPA with React/Redux.
All ideas, references, UI/UX and feature tools can be founded can be found on this link [Trello](https://trello.com/b/592LyGRy).


Below you will find some information on how to perform common tasks.<br/>

## Folder Structure

The Web App consist of two independent npm packages:

```
backend/
frontend/
README.md
```


Sorry, but backend will be not run without postgresql db!!!
I attached db schema in /backend/DB_SCHEMA.


Before install all dependencies in frontend and backend folder

### `npm install`

And then runs the frontend side in the development mode.<br/>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


For the project to build, **these files must exist with exact filenames**:

* `frontend/public/index.html` is the page template;
* `frontend/src/index.js` is the JavaScript entry point.


## Available Scripts

In the frontend project directory, you can run:

### `npm start`

Runs the app in the development mode.<br/>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br/>
You will also see any lint errors in the console.



## Proxying API Requests in Development
To tell the development server to proxy any unknown requests to API server in development:

```js
  "proxy": "http://localhost:3001",
```






