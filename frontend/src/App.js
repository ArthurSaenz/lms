import React, { Component } from 'react';
import { Layout } from 'antd';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
// COMPONENTS PAGES
import WeeklyApp from './WeeklySchedule/WeeklySchedule';
import AddPupilContainer from './Pupils/Components/AddPupil/index';
import SidebarMenu from './Sidebar/SidebarMenu';
import Header from './Header/HeaderContainer';
import TableAntD from './Pupils/ContainerPupils';
import HomePageContainer from './HomePage/HomePageContainer.js';
import DepartmentsContainer from './Departments/DepartmentsContainer';
import { ModalRootComponent } from './Modals/ModalRootComponent';


const ContentWrap = styled(Layout.Content)`
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    margin: 5em 1em 1.8em 1em;
    padding: 2rem;
    background: #fff;

    @media (max-width: 992px) {
        margin: 5em 0 1em 0;
        padding: .5em; 
    }
`;


const LayoutWrap = styled(Layout)`
    transition: all 0.2s;
    min-height: 100%;
    ${props => props.collapsed=='true' ? ' margin-left: 80px' : 'margin-left: 200px'};

    @media (max-width: 992px) {        
        margin-left: 0;
    }
`;


const Main = () => (
    <div className="content-wrapper content-main">
        <Switch>
            <Route exact path='/' component={HomePageContainer} />            
            <Route path='/pupils' component={TableAntD} />
            <Route path='/addpupil' component={AddPupilContainer}/>
            <Route path='/teachers' component={WeeklyApp} />
            <Route path='/departments' component={DepartmentsContainer} />
        </Switch>
    </div>       
);


class App extends Component {
    constructor(props) {
        super(props);        
        this.state = {
            collapsed: false,
            isBreakpoint: false,
            activeKey: []
        };
    }


    toggleIsMobile = broken => this.setState({ isBreakpoint: broken});
    toggleSidebar = () => this.setState( prevState => ({ collapsed: !prevState.collapsed}));
    toogleOpenKey = (key) => this.setState({activeKey: [key]});
    

    render() {
        console.log('render app component ', this.state);
        return (
            <LayoutWrap collapsed={this.state.collapsed ? 'true' : 'false'} >
                <SidebarMenu toogleOpenKey={this.toogleOpenKey} activeKey={this.state.activeKey} collapsed={this.state.collapsed} isBreakpoint={this.state.isBreakpoint} toggleSidebar={this.toggleSidebar} toggleIsMobile={this.toggleIsMobile}/>
                <Layout>                
                    <Header collapsed={this.state.collapsed} toggleSidebar={this.toggleSidebar}/>
                    <ContentWrap>
                        <Main/>
                    </ContentWrap>
                </Layout>
                <ModalRootComponent/>
            </LayoutWrap>
        )
    };
}


export default App;
