import * as el from '../constants/modal';
import axios from 'axios';
import { message } from 'antd';


// General actions

export const hideModal = () => ({ type: el.HIDE_MODAL });


export const destroyModal = () => {
    return {
        type: el.DESTROY_MODAL
    };
}


export const showModal = (modalType, modalProps) => {    
    return {
        type: el.SHOW_MODAL,
        modalProps: modalProps,
        modalType: modalType
    }
};


export const mapModalProps = props => {
    return {
        type: el.MODAL_PROPS,
        modalProps: props
    }
}


// DeleteComponent actions
export const excludePupils = props => {
    return 
}


const delay = ms => new Promise(resolve => setTimeout(resolve, ms));


export const testAction = () => async (dispatch) => {
    dispatch(hideModal());
    const hide = message.loading('Action in progress..', 0);
    try {
        const resultFetch = await delay(3000)
        // throw 42
        hide()
        message.success('This is a message of success');
    } catch (e) {
        hide()
        message.error('This is a message of error');
    }
};


// not completed
export const deletePupils = (ids) => async (dispatch) => {
    dispatch();
    const hide = message.loading('Action in progress..', 0);
    try {
        const result = await axios.delete();
        dispatch()
        hide()
        message.success('This is a message of success');
    } catch (e) {
        console.log(e);        
        dispatch()
        hide()
        message.error('This is a message of error');
    }     
};


// ModalPupil Wrapper


// old version
export const handleCancel = () => {    
    return {
        type: el.HIDE_MODAL,
    };
}


export const showModalPupil = (dataId) => {
    return {
        type: el.SHOW_MODAL,
        // modalType: typeModal,
        modalProps: {
            id: dataId
        },
        visible: true
    };
}


export const handleOkModal = () => {
    console.log('Modal is OK')
    // async function with hide modal + FETCH to server
    // confirmLoading action
    return {
        type: el.HIDE_MODAL
    }
};