import * as el1 from '../constants/addpupilform';
import axios from 'axios';
import { push } from 'connected-react-router';

export const stepformValue = data => {
    return {
        type: el1.STEP1_VALUE_SAVE,
        data: data
    }
};


export const isLoading = () => ({
   type: el1.IS_LOADING_REQUEST
});


export const isFailure = () => ({
   type: el1.IS_FAILURE
});


export const isSuccessRequest = () => ({
   type: el1.IS_SUCCESS_REQUEST
});


export const clearFieldsValue = () => ({
    type: el1.CLEAR_FIELDS_VALUE
});


export const fetchDataNewPupil = (values, pathChange) => async (dispatch) => {
    dispatch(isLoading());
    try {
        const newPupil = await axios.post('/addpupil', values);
        dispatch(isSuccessRequest());
        dispatch(push(pathChange))
    } catch (e) {
        console.log(e);
        dispatch(isFailure())
    }
};