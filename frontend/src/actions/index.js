import { fetchTeachers } from './teachers';
import { fetchPupils } from './pupils';
import { fetchDepartments } from './departments';



export const fetchDefaultData = () => {
    return dispatch =>
        Promise.all([dispatch(fetchPupils()), dispatch(fetchTeachers()), dispatch(fetchDepartments())]);
};


