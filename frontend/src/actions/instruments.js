import * as el from '../constants/instruments';
import axios from 'axios';


export const instrumentsItemsFetchDataSuccess = items => {
    return {
        type: el.INSTRUMENTS_ITEMS_SUCCESS,
        items: items,
        isLoading: false
    };
}

export const instrumentsItemsIsLoading = () => ({ type: el.DEPARTMENT_IS_LOADING, isLoading: true});


export const instrumentsItemsHasError = () => ({type: el.DEPARTMENT_HAS_ERROR, hasError: true});


export const instrumentRemove = id => {
    return {
        type: el.INSTRUMENT_REMOVE,
        id: id,
        isLoading: false
    };
}


export const fetchInstruments = () => async (dispatch) => {
    dispatch(instrumentsItemsIsLoading());
    try {
        const instruments = await axios.get("/departments");
        // possibly change fetchData
        dispatch(departmentsItemsFetchDataSuccess(departments));
    } catch (e) {
        console.log(e);
        dispatch(instrumentsItemsHasError());
    }
};