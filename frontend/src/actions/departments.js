import * as el from '../constants/departments';
import axios from 'axios';


export const departmentsItemsFetchDataSuccess = items => {
    return {
        type: el.DEPARTMENTS_ITEMS_SUCCESS,
        items: items,
        isLoading: false
    };
}

export const departmentsItemsIsLoading = () => ({ type: el.DEPARTMENTS_ITEMS_IS_LOADING, isLoading: true});


export const departmentsItemsHasError = () => ({type: el.DEPARTMENTS_ITEMS_HAS_ERROR, hasError: true});


export const departmentRemove = id => {
    return  {
        type: el.DEPARTMENT_REMOVE,
        id: id,
        isLoading: false
    }
}
 

export const fetchDepartments = () => async (dispatch) => {
    dispatch(departmentsItemsIsLoading());
    try {
        const departments = await axios.get("/departments");
        // possibly change fetchData
        dispatch(departmentsItemsFetchDataSuccess(departments));
    } catch (e) {
        console.log(e);
        dispatch(departmentsItemsHasError());
    }
};


export const fetchDepartmentRemove = (id) => async (dispatch) => {

}; 


export const fetchDepartmentAdd = (item) => async (dispatch) => {
    dispatch(departmentsItemsIsLoading());
    try {
        const departmentId = await axios.post('/department/', item);
        const newDepartment = { byId: {[departmentId] : item} , allIds: [departmentId]};
        dispatch(departmentsItemsFetchDataSuccess(newDepartment));
    } catch (e) {
        console.log(e);
        dispatch(departmentsItemsHasError());
    }
};


export const fetchDepartment = (id) => async (dispatch) => {
    dispatch(departmentsItemsIsLoading());
    try {
        const department = await axios.get("/department/" + id);
        // possibly change fetchData
        dispatch(departmentsItemsFetchDataSuccess(department));
    } catch (e) {
        console.log(e);
        dispatch(departmentsItemsHasError());
    }
};