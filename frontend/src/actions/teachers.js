import * as el from '../constants/teachers';
import axios from 'axios';


export function teachersItemsHasError() {
    return {
        type: el.TEACHERS_ITEMS_HAS_ERROR,
        hasError: true
    };
}


export function teachersItemsIsLoading() {
    return {
        type: el.TEACHERS_ITEMS_IS_LOADING,
        isLoading: true,
        hasError: false
    };
}


export function teachersItemsFetchDataSuccess(items) {
    return {
        type: el.TEACHERS_ITEMS_SUCCESS,
        items: items,
        isLoading: false
    };
}
 

export const fetchTeachers = () => async (dispatch) => {
    dispatch(teachersItemsIsLoading());
    try {
        const teachers = await axios.get("/teachers");
        dispatch(teachersItemsFetchDataSuccess(teachers));
    } catch (e) {
        console.log(e);
        dispatch(teachersItemsHasError());
    }
};