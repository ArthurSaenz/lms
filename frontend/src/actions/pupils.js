import * as el from '../constants/pupils';
import axios from 'axios';


export const pupilsItemsIsLoading = () => ({ type: el.PUPILS_ITEMS_IS_LOADING, isLoading: true});


export const pupilsItemsHasError = () => ({type: el.PUPILS_ITEMS_HAS_ERROR, hasError: true});


export const pupilRemove = id => {
    return  {
        type: el.PUPIL_REMOVE,
        id: id,
        isLoading: false
    }
}


export const pupilsItemsFetchDataSuccess = items => {
    console.log('axios data ', items);
    return {
        type: el.PUPILS_ITEMS_SUCCESS,
        items: items,
        isLoading: false
    };
}

// export function pupilsItemsFetchData(url) {
//     return (dispatch) => {
//         dispatch(pupilsItemsIsLoading(true));
//         fetch(url)
//             .then((response) => {
//                 if (!response.ok) {
//                     throw Error(response.statusText)
//                 }

//                 dispatch(pupilsItemsIsLoading(false));

//                 return response
//             })

//             .then((response) => response.json())
//             .then ((items) => {
//                 dispatch(pupilsItemsFetchDataSuccess(items));
//                 console.log('pupils succees fetch: ', items)
//             })
//             .catch(() => dispatch(pupilsItemsHasErrored(true)))
//     };
// }


export const fetchPupils = () => async (dispatch) => {
    dispatch(pupilsItemsIsLoading());
    try {
        const pupils = await axios.get("/pupils");
        dispatch(pupilsItemsFetchDataSuccess(pupils.data));
    } catch (e) {
        console.log(e);
        dispatch(pupilsItemsHasError());
    }
};

