export const PUPILS_ITEMS_HAS_ERROR = 'PUPILS_ITEMS_HAS_ERROR';
export const PUPILS_ITEMS_IS_LOADING = 'PUPILS_ITEMS_IS_LOADING';
export const PUPILS_ITEMS_SUCCESS = 'PUPILS_ITEMS_SUCCESS';
export const PUPIL_REMOVE = 'PUPIL_REMOVE';