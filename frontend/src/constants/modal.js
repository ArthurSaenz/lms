export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const DESTROY_MODAL = 'DESTROY_MODAL';
export const MODAL_PROPS = 'MODAL_PROPS';



export const PUPIL_MODAL = 'PUPIL_MODAL';
export const TEACHER_MODAL = 'TEACHER_MODAL';
export const DELETE_PUPIL = 'DELETE_PUPIL';
