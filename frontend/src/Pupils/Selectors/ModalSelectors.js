import { createSelector } from 'reselect';


const getTeachers = (state) => state.teachers;
const getDepartments = (state) => state.departments;
const getInstruments = (state) => state.instruments;

export const getDataDepartmentsTeachers = createSelector(
    [getDepartments, getTeachers, getInstruments],
    (departments, teachers, instruments) => departments.allIds.map(el => {
        return {
            label: departments.byId[el].name,
            value: departments.byId[el].name,
            children: departments.byId[el].teachers.map( el => ({
                value: teachers.byId[el].name,
                label:  teachers.byId[el].name,                
                children: teachers.byId[el].instruments.map( el => ({
                    value: instruments.byId[el].name,
                    label: instruments.byId[el].name
                }))
            }))
        }
    })
)