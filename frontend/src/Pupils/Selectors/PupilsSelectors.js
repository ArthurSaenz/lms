import { createSelector } from 'reselect';


const getPupils = (state) => state.pupils;
const getTeachers = (state) => state.teachers;
const getDepartment = (state) => state.departments;

export const getPupilsItems = createSelector(
    [ getPupils, getTeachers, getDepartment ],
    (pupils, teachers, departments) => pupils.allIds
        .map(key => Object.assign({},
            {key: pupils.byId[key].id}, 
            {name: pupils.byId[key].name}, 
            {teacher: teachers.byId[pupils.byId[key].teacher].name},
            {level: pupils.byId[key].level}, 
            {sixth: pupils.byId[key].sixth}, 
            {department: departments.byId[teachers.byId[pupils.byId[key].teacher].department].title} 
        )            
    )
);

// dont test