import React from 'react';
import { handleOkModal, handleCancel } from '../../actions/modal';
import { connect } from 'react-redux';
import { ModalPupilWrapper } from './ModalPupilWrapper';
import { bindActionCreators } from 'redux';
import { getDataDepartmentsTeachers } from '../Selectors/ModalSelectors';



const getDataIdPupil = (id, state) => {
    if (id === undefined) {
        return null
    } else { 
        const pupil = state.pupils.byId[id];
        return Object.assign({}, pupil, {
            teacher: state.teachers.byId[pupil.teacher].name,
            department: state.departments.byId[state.teachers.byId[pupil.teacher].department].name,
            instruments: pupil.instruments.map( el => state.instruments.byId[el].name)
        })
    }
};



function mapStateToProps (state, ownProps) {
    console.log('props testFunc', getDataDepartmentsTeachers(state))
    return {
        visibleState: state.modal.modalProps.visible,        
        dataPupil: getDataIdPupil(state.modal.modalProps.id, state),
        dataDepartmentsTeachers: getDataDepartmentsTeachers(state), // selector 
        ...ownProps
    };
}


function mapDispatchToProps(dispatch) {
    return {
        handleCancel: bindActionCreators(handleCancel, dispatch),
        handleOk: bindActionCreators(handleOkModal, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalPupilWrapper);