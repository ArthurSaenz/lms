import React from "react";
import styled from 'styled-components';
import { Icon, Button, Row, Col } from 'antd';

const Fragment = React.Fragment;


const Wrapper = styled.div`
    text-align: center;
    width: 72%;
    margin: 0 auto;
    padding: 2em;
    @media screen and (max-width: 700px) {
        width: 100%;
    }
    
    button:not(:last-child) {
        margin-right: 1em;
    }
`;


const IconNew = styled(Icon)`
    font-size: 72px;
    color: green;
    line-height: 72px;
    margin-bottom: 24px;
`;


const Title = styled.h1`
    font-size: 2em;
    color: @heading-color;
    font-weight: 500;
    line-height: 2.5em;
    margin-bottom: 1em;
`;


const DescriptionTitle = styled.h2`
    
`;

// componentWillUnmount() {
//     this.props.clearFieldsValue()
// };

const Step4Form = (props) => {

    console.log('Props from store ', props.data );

    const onFinish = () => props.changeRoute('/');
    const onAddNewPupil = () => props.changeRoute('/addpupil/');

    // const descriptionInformation = (
    //     <div>
    //         <Row>
    //             <Col xs={24} sm={8}>
    //                 Name child：
    //             </Col>
    //             <Col xs={24} sm={16}>
    //                 {data.nameChild}
    //             </Col>
    //         </Row>
    //         <Row>
    //             <Col xs={24} sm={8}>
    //                 Surname child：
    //             </Col>
    //             <Col xs={24} sm={16}>
    //                 {data.surnameChild}
    //             </Col>
    //         </Row>
    //         {data.fields.map(el => {
    //             return (
    //                 <Fragment>
    //                     <Row>
    //                         <Col xs={24} sm={8}>
    //                             Instrument:
    //                         </Col>
    //                         <Col xs={24} sm={16}>
    //                             {el.instrument}
    //                         </Col>
    //                     </Row>
    //                     <Row>
    //                         <Col xs={24} sm={8}>
    //                             Teacher：
    //                         </Col>
    //                         <Col xs={24} sm={16}>
    //                             <span>{el.teacher}</span> 元
    //                         </Col>
    //                     </Row>
    //                     <Row>
    //                         <Col xs={24} sm={8}>
    //                             Level：
    //                         </Col>
    //                         <Col xs={24} sm={16}>
    //                             <span>{el.number + ' of ' + el.currency}</span> 元
    //                         </Col>
    //                     </Row>
    //                 </Fragment>
    //             )
    //         })}
    //
    //         <Row>
    //             <Col xs={24} sm={8}>
    //                 Price for month：
    //             </Col>
    //             <Col xs={24} sm={16}>
    //                 <span>{500}</span> 元
    //             </Col>
    //         </Row>
    //     </div>
    // );

    return (
        <Wrapper>
            <IconNew type="check-circle" />
            <Title>Success!</Title>
            <DescriptionTitle></DescriptionTitle>

            <Button type='primary' onClick={onFinish}>Home</Button>
            <Button onClick={onAddNewPupil}>Add one pupil</Button>

        </Wrapper>
    )
};

export default Step4Form;