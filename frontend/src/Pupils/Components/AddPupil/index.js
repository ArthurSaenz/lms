import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Steps, Row, Col, Form } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import * as actions from '../../../actions/addpupilform';
import Step1Form from './Step1';
import Step2Form from './Step2';
import Step3Form from './Step3';
import Step4Form from './Step4';
import { WrapperCol } from "./Components/styledComponents";


const Step = Steps.Step;


const steps = [
    {
        title: 'First',
        content: 'First-content',
    }, {
        title: 'Second',
        content: 'Second-content',
    }, {
        title: 'Last',
        content: 'Last-content',
    }, {
        title: 'Final',
        content: 'Done!'
    }
];


const routesPath = ['/addpupil', '/addpupil/step2', '/addpupil/step3', '/addpupil/step4'];



const currentPathToSteps = (path, arrayOfArrays) => {
    switch (path) {
        case arrayOfArrays[0]:
            return 0;
        case arrayOfArrays[1]:
            return 1;
        case arrayOfArrays[2]:
            return 2;
        case arrayOfArrays[3]:
            return 3;
    }
};


const AddPupilContainer = props => {
    return (
        <Row>
            <WrapperCol xl={18} sm={24}>
                    <Switch>
                        <Route exact path='/addpupil/' render={() => <Step1Form {...props}/>}/>
                        <Route path='/addpupil/step2' render={() => <Step2Form {...props}/>}/>
                        <Route path='/addpupil/step3' render={() => <Step3Form {...props}/>}/>
                        <Route path='/addpupil/step4' render={() => <Step4Form {...props}/>}/>
                    </Switch>
            </WrapperCol>
            <WrapperCol xl={6} sm={24}>
                    <Steps direction="vertical" current={currentPathToSteps(props.pathname, routesPath)}>
                        {steps.map((el, num) => <Step key={num} title={el.title} description={el.content}/>)}
                    </Steps>
            </WrapperCol>
        </Row>
    ) 
};


const mapStateToProps = (state) => ({
    pathname: state.router.location.pathname,
    data: state.addPupilForm.valuesFields,
    loading: state.addPupilForm.isLoadingRequest
});


const mapDispatchToProps = (dispatch) => ({
    saveStepValue: bindActionCreators(actions.stepformValue, dispatch),
    fetchDataNewPupil: bindActionCreators(actions.fetchDataNewPupil, dispatch),
    clearFieldsValue: bindActionCreators(actions.clearFieldsValue, dispatch),
    changeRoute: (path) => dispatch(push(path))
});


export default connect(mapStateToProps, mapDispatchToProps)(AddPupilContainer);
