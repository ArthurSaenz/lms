import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, InputNumber, DatePicker, Form, Input, Col, Row } from 'antd';
import { FormItem, Title } from "./Components/styledComponents";


const Fragment = React.Fragment;


const Form1 = (props) => {

    const { form, data, saveStepValue, changeRoute } = props;
    const { getFieldDecorator, validateFields } = form;   

    const validateFormSubmit = (e) => {
        validateFields((err, value) => {
            if (!err) {
                saveStepValue(value);
                changeRoute('/addpupil/step2')
            }
        })
    };

    return (
        <Fragment>
            <Title>Pupil Information</Title>
            <Form>

                <Row gutter={32}>
                    <Col xl={12} md={24}>
                        <FormItem label="Name">
                            {getFieldDecorator('nameChild', {
                                initialValue: data.nameChild,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    </Col>
                    <Col xl={12} md={24}>
                        <FormItem label='Surname'>
                            {getFieldDecorator('surnameChild', {
                                initialValue: data.surnameChild,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    </Col>
                </Row>


                <Row gutter={32} >
                    <Col xl={12} md={24}>
                        <FormItem label="Birthday (like DD-MM-YYYY)">
                            {getFieldDecorator('birthday', {
                                initialValue: data.birthday,
                                rules: [{ type: 'object', required: true, message: 'Please select day!' }],
                            })(
                                <DatePicker size="large" format="DD-MM-YYYY"/>
                            )}
                        </FormItem>
                    </Col>
                    <Col xl={12} md={24}>
                        <FormItem label="Number of child's phone">
                            {getFieldDecorator('numberChild', {
                                initialValue: data.numberChild
                            })(
                                <Input type="tel" size="large" />
                            )}
                        </FormItem>
                    </Col>
                </Row>



                <Row gutter={32}>
                    <Col xl={8} md={18} >
                        <FormItem label="Basic school / Kindergarten">
                            {getFieldDecorator('mainSchool', {
                                initialValue: data.mainSchool,
                                rules: [{ required: true }]
                            })(
                                <Input  size="large" type="textarea" />
                            )}
                        </FormItem>
                    </Col>
                    <Col xl={4} md={6}>
                        <FormItem label="Level in school">
                            {getFieldDecorator("levelSchool", {
                                initialValue: data.levelSchool
                            })(
                                <InputNumber size="large"  min={0} max={12} />
                            )}
                        </FormItem>
                    </Col>
                    <Col xl={12} md={24}>
                        <FormItem label="Living address">
                            {getFieldDecorator('addressChild', {
                                initialValue: data.addressChild,
                                rules: [{ required: true }]
                            })(
                                <Input  size="large" type="textarea" placeholder="Address of living child" />
                            )}
                        </FormItem>
                    </Col>
                </Row>


                <Row type="flex" justify="end">
                    <Col>
                        <Button type="primary" size='large' onClick={validateFormSubmit}>
                            Go forward<Icon type="right" />
                        </Button>
                    </Col>
                </Row>


            </Form>
        </Fragment>
    )
};


Form1.propTypes = {
    data: PropTypes.shape({
        nameChild: PropTypes.string,
        surnameChild: PropTypes.string,
        birthday: PropTypes.object,
        addressChild: PropTypes.string,
        numberChild: PropTypes.number,
        mainSchool: PropTypes.string,
        levelSchool: PropTypes.number
    })
};



const Step1Form = Form.create()(Form1);

export default Step1Form;