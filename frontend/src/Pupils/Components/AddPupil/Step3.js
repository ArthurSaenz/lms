import React from "react";
import PropTypes from 'prop-types';
import { Button, Icon, DatePicker, Form, Col, Row,  Checkbox, Input, Upload, Spin } from 'antd';
import { FormItem, Title, FormItemTypes, FormItemLargeMargin, FromItemSmallMargin } from "./Components/styledComponents";
import { FieldsTypeEducation, checkTypeEducationFunc } from './Components/FieldsTypeEducation';


const MonthPicker = DatePicker.MonthPicker;

const { TextArea } = Input;


const demoTypeProps = {
    instrument: null,
    teacher: null,
    number: 1,
    currency: '8th'
};

let uuid = 1;

const Form3 = (props) => {
    const { form, data, changeRoute, loading, saveStepValue, fetchDataNewPupil } = props;
    const { getFieldDecorator, validateFields, getFieldValue } = form;

    const onPrev = () => changeRoute('/addpupil/step2');

    // async dispatch submit btn
    const validateFinalFormSubmit = (e) => {
        validateFields((err, value) => {            
            if (!err) {
                // first get valueFields to store
                saveStepValue(value);
                // then do fetch full store
                console.log('data form', data);
                fetchDataNewPupil(form, '/addpupil/step4')
                // changeRoute('/addpupil/step4')
            }
        })
    };


    // methods for dynamic formItems
    const remove = (k) => {        
        const keys = form.getFieldValue('keys');
        // We need at least one 
        if (keys.length === 1) {
            return;
        }

        // can use data-binding to set 
        form.setFieldsValue({
            keys: keys.filter(key => key !== k),
        });
    };


    const add = () => {
        // can use data-binding to get
        const keys = form.getFieldValue('keys');
        const nextKeys = keys.concat(uuid);
        uuid++;
        // can use data-binding to set
        // important! notify form to detect changes
        form.setFieldsValue({
            keys: nextKeys,
        })
    };

       
    const normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };


    // Dynamic Form item for select multiple changes
    getFieldDecorator('keys', { initialValue: [0] });
    const keys = getFieldValue('keys');    


    const formItemsMultiple = keys.map((k, index) => {
        return (
            <FormItemTypes                
                required={false}
                key={k}
            >
                    {getFieldDecorator(`fields[${k}]`, {
                        validateTrigger: ['onChange', 'onBlur'],
                        initialValue: demoTypeProps,
                        rules: [{
                            type: 'object',
                            required: true,                            
                            validator: checkTypeEducationFunc,
                            whitespace: true,
                        }],
                    })(
                        <FieldsTypeEducation keys={keys} index={index} onClickRemove={() => remove(k)}/>
                    )}
            </FormItemTypes>
        )
    });

    return(
        <Spin spinning={loading}>
            <Title>Education Information</Title>
            <Form>
                <Row>
                    <Col xl={5} md={8}>
                        Instrument:
                    </Col>
                    <Col xl={5} md={8}>
                        Teacher:
                    </Col>
                    <Col xl={10} md={8}>
                        Level:
                    </Col>
                </Row>
                {formItemsMultiple}
                {keys.length <= 2 ?
                    <FormItem>
                        <Button type="dashed" onClick={add}>
                            <Icon type="plus" /> Add field
                        </Button>
                    </FormItem>
                    :
                    null
                }



                <Row gutter={32}>
                    <Col lg={8} md={12}>
                        <FormItem label="Start of education">
                            {getFieldDecorator('startEducation', {
                                rules: [{ type: 'object', required: true, message: 'Please select month of start education!' }],
                            })(
                                <MonthPicker size="large" />
                            )}
                        </FormItem>
                    </Col>
                    <Col lg={8} md={12}>
                        <FormItem
                            label="Upload Document"
                        >
                            {getFieldDecorator('upload', {
                                valuePropName: 'fileList',
                                getValueFromEvent: normFile,
                            })(
                                <Upload name="logo" action="/upload.do" listType="picture">
                                <Button size="large">
                                    <Icon type="upload" /> Click to upload
                                </Button>
                                </Upload>
                            )}
                        </FormItem>
                    </Col>
                </Row>

                <Row gutter={32}>
                    <Col lg={16} md={24}>
                        <FormItemLargeMargin label='Comment'>
                            {getFieldDecorator('comment')(
                                <TextArea autosize={{ minRows: 2, maxRows: 3 }} />
                            )}
                        </FormItemLargeMargin>
                    </Col>
                </Row>

                <Row type="flex" justify="center">
                    <Col lg={12} md={24}>
                            <FromItemSmallMargin>
                                {getFieldDecorator('isInstrument', {
                                    initialValue: false                                
                                })(
                                    <Checkbox size="large">Do You have musical instrument?</Checkbox>
                                )}
                            </FromItemSmallMargin>
                    </Col>
                </Row>

                <Row type="flex" justify='center'>
                    <Col lg={12} md={24}>
                        <FormItem
                            extra="We must make sure that you read the rules."
                        >
                            {getFieldDecorator('agree', {
                                initialValue: false,
                                rules: [{ required: true, message: 'Please agree the rules'}]
                            })(
                                <Checkbox>I have read the <a href="">agreement</a></Checkbox>
                            )}
                        </FormItem>
                    </Col>
                </Row>


                <Row type="flex" justify="end">
                    <Col>
                        <Button.Group size='large'>
                            <Button type='primary' onClick={onPrev}>
                                <Icon type='left' />Backward
                            </Button>
                            <Button type='primary' disabled={!form.getFieldValue('agree')} onClick={validateFinalFormSubmit}>
                                Forward<Icon type='user-add' />
                            </Button>
                        </Button.Group>

                    </Col>
                </Row>

            </Form>
        </Spin>
    )
};


Form3.propTypes = {
    data: PropTypes.shape({
        teacher: PropTypes.arrayOf(PropTypes.string),
        fields: PropTypes.arrayOf(
            PropTypes.shape({
                instrument: PropTypes.string,
                teacher: PropTypes.string,
                number: PropTypes.number,
                currency: PropTypes.oneOf(['6th', '8th'])
            })
        ),
        startEducation: PropTypes.object,
        comment: PropTypes.string,
        isInstrument: PropTypes.bool
    })
};


const Step3Form = Form.create()(Form3);

export default Step3Form;