import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, Form, Input, Col, Row } from 'antd';
import { FormItem, Title, TitleH3 } from "./Components/styledComponents";


const Fragment = React.Fragment;


const Form2 = (props) => {
    const { form, data, changeRoute, saveStepValue } = props;
    const { getFieldDecorator, validateFields } = form;
    
    const onPrev = () => changeRoute('/addpupil');

    const validateFormSubmit = (e) => {
        validateFields((err, value) => {
            if (!err) {
                saveStepValue(value);
                changeRoute('/addpupil/step3')
            }
        })
    };


    return(
        <Fragment>
            <Title>Parents Information</Title>
            <Form>
                <TitleH3>Mother Information</TitleH3>
                <Row gutter={32}>
                    <Col lg={12} md={24}>
                        <FormItem label="Name mother">
                            {getFieldDecorator('nameMother', {
                                initialValue: data.nameMother,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    </Col>
                    <Col lg={12} md={24}>
                        <FormItem label='Surname mother'>
                            {getFieldDecorator('surnameMother', {
                                initialValue: data.surnameMother,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    </Col>
                </Row>

                <Row gutter={32}>
                    <Col lg={12} md={24}>
                        <FormItem label="Number Phone">
                            {getFieldDecorator('numberMother', {
                                initialValue: data.numberMother,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input type="tel" size="large" />
                            )}
                        </FormItem>
                    </Col>
                    <Col lg={12} md={24}>
                        <FormItem label="Mother's E-mail">
                            {getFieldDecorator('emailMother', {
                                initialValue: data.emailMother,
                                rules: [{
                                    type: 'email', message: 'The input is not valid E-mail!',
                                }, {
                                    required: true, message: 'Please input your E-mail!',
                                }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    </Col>
                </Row>

                {/*<Row gutter={32}>*/}
                    {/*<Col span={12}>*/}
                        <FormItem label="Mother's work">
                            {getFieldDecorator('workMother', {
                                initialValue: data.workMother,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    {/*</Col>*/}
                {/*</Row>*/}



                <TitleH3>Father Information</TitleH3>
                <Row gutter={32}>
                    <Col lg={12} md={24}>
                        <FormItem label="Name mother">
                            {getFieldDecorator('nameFather', {
                                initialValue: data.nameFarther ,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    </Col>
                    <Col lg={12} md={24}>
                        <FormItem label='Surname mother'>
                            {getFieldDecorator('surnameFarther', {
                                initialValue: data.surnameFarther,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    </Col>
                </Row>

                <Row gutter={32}>
                    <Col lg={12} md={24}>
                        <FormItem label="Number Phone">
                            {getFieldDecorator('numberFarther', {
                                initialValue: data.numberFarther ,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input type="tel" size="large" />
                            )}
                        </FormItem>
                    </Col>
                    <Col lg={12} md={24}>
                        <FormItem label="Farther's E-mail">
                            {getFieldDecorator('emailFarther', {
                                initialValue: data.emailFarther,
                                rules: [{
                                    type: 'email', message: 'The input is not valid E-mail!',
                                }, {
                                    required: true, message: 'Please input your E-mail!',
                                }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    </Col>
                </Row>


                {/*<Row gutter={32}>*/}
                    {/*<Col span={12}>*/}
                        <FormItem label='Farther Work'>
                            {getFieldDecorator('workFarther', {
                                initialValue: data.workFarther,
                                rules: [{ required: true, message: 'Please input the title of collection!' }],
                            })(
                                <Input size="large" />
                            )}
                        </FormItem>
                    {/*</Col>*/}
                {/*</Row>*/}


                <Row type="flex" justify="end">
                    <Col>
                        <Button.Group size='large'>
                            <Button type='primary' onClick={onPrev}>
                                <Icon type='left' />Backward
                            </Button>
                            <Button type='primary' onClick={validateFormSubmit}>
                                Forward<Icon type='right' />
                            </Button>
                        </Button.Group>

                    </Col>
                </Row>


            </Form>
        </Fragment>
    )
};


Form2.propTypes = {
    data: PropTypes.shape({
        nameMother: PropTypes.string,
        surnameMother: PropTypes.string,
        numberMother: PropTypes.number,
        emailMother: PropTypes.string,
        workMother: PropTypes.string,
        nameFarther: PropTypes.string,
        surnameFarther: PropTypes.string,
        numberFarther: PropTypes.number,
        emailFarther: PropTypes.string,
        workFarther: PropTypes.string
    })
};



const Step2Form = Form.create()(Form2);


export default Step2Form;