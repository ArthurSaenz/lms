import { Col, Form, Icon } from "antd";
import styled from "styled-components";


export const FormItem = styled(Form.Item)`
    &&& {
        height: 6em;
        margin-bottom: 1em;        
    }
`;

export const FormItemLargeMargin = FormItem.extend`
    &&& {
        height: 8em;
    }
`;

export const FromItemSmallMargin = FormItem.extend`
    &&& {
        height: 2em;
    }
`

export const FormItemTypes = styled(Form.Item)`
    &&& {
        height: auto;
        // 5em
        margin-bottom: 1em;        
    }

    @media (max-width: 992px) {
        &&& {
            height: auto;            
            margin-bottom: 1em;        
        }
    
    }
`;


export const Title = styled.h2`
    margin-bottom: 1em;
`;


export const WrapperCol = styled(Col)`
    &&& {
        padding: 1.5em 2.5em;
    }
`;

export const TitleH3 = styled.h3`
    font-weight: 600;
    margin-bottom: 1.3rem;
`;


export const IconRemove = styled(Icon)`
    cursor: pointer;
    position: relative;
    top: .25em;
    font-size: 1.7em;
    color: #999;
    transition: all .3s;

    &:hover {
        color: #777;
    }
`;