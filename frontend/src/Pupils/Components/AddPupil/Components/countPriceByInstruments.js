const priceList = {
    Piano: 170,
    String: 150,
    Guitar: 170,
    Vocal: 160
};

const instruments = ['Piano', 'String', 'Guitar', 'Vocal'];

const checkPriceByInstruments = (priceObject, fields = []) => fields.map(el => priceList[el.instrument]).reduce((el1, el2) => el1 + el2, 0);