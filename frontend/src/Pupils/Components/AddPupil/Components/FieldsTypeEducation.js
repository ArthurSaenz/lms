import React, { Component } from "react";
import { Select, Cascader, InputNumber, Row, Col } from "antd";
import { IconRemove, FormItem } from "./styledComponents";

const Option = Select.Option;

const Fragment = React.Fragment;


const dataOfTeachers = [
    {
        value: "Piano",
        label: "Piano",
        children: [
            {
                value: "hangzhou",
                label: "Hangzhou",
                children: [
                    {
                        value: "Piano",
                        label: "Piano"
                    }
                ]
            }
        ]
    },
    {
        value: "jiangsu",
        label: "Jiangsu",
        children: [
            {
                value: "nanjing",
                label: "Nanjing",
                children: [
                    {
                        value: "zhonghuamen",
                        label: "Zhong Hua Men"
                    }
                ]
            }
        ]
    }
];


export const checkTypeEducationFunc = (rule, value, callback) => {
    if (value.currency === '8th') {
        if (value.number > 8) {
            callback('In 8th type of education maximum level is 8')
        }
                
    } else {
        if (value.number > 6) {
            callback('In 6th type of education maximum level is 6')
        }
        if (value.number === 0) {
            callback('Level #0 must be in 8th type')
        }
     
    }

    if (value.instrument == null) {
        callback('Please select the instrument')
        return;
    };
    callback();
    return    
};

export const priceList = {
    Piano: 170,
    String: 150,
    Guitar: 170,
    Vocal: 160
};

export const instruments = ['Piano', 'String', 'Guitar', 'Vocal'];
export const teachers = {
    Piano: ['Hangzhou', 'Ningbo', 'Wenzhou'],
    String: ['Nanjing', 'Suzhou', 'Zhenjiang'],
    Guitar: ['Kate', 'John'],
    Vocal: ['Michael', 'James', 'David']
};


export class FieldsTypeEducation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            instrument: this.props.value.instrument,
            teacher: this.props.value.teacher,
            number: this.props.value.number,
            currency: this.props.value.currency,
        };
    }

    componentWillReceiveProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            const value = nextProps.value;
            this.setState(value)
        }
    };


    handleNumberChange = (e) => {
        const number = parseInt(e || 0, 10);
        if (isNaN(number)) {
            return;
        }
        if (!('value' in this.props)) {
            this.setState({ number });
        }
        this.triggerChange({ number })
    };


    handleCurrencyChange = (currency) => {
        if (!('value' in this.props)) {
            this.setState({ currency });
        }
        this.triggerChange({ currency })
    };


    handleInstrumentChange = (instrument) => {
        this.setState({ instrument: instrument, teacher: null});
        this.triggerChange({ instrument })
    };

    handleTeacherChange = (teacher) => { 
        this.setState({ teacher: teacher });
        this.triggerChange({ teacher })
    };


    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(Object.assign({}, this.state, changedValue))
        }
    };

    
    
    render() {
        const { onClickRemove, keys, index } = this.props;
        const state = this.state;

        const instrumentsOptions = instruments.map( instrument => <Option key={instrument}>{instrument}</Option>);
        const teacherOptions = state.instrument ? teachers[state.instrument].map( teacher => <Option key={teacher}>{teacher}</Option>) : null;


        return (
            <Fragment>
                <Row gutter={16}>
                    <Col xl={5} md={24}>
                        <FormItem>
                            <Select size="large" value={state.instrument} onChange={this.handleInstrumentChange}>
                                {instrumentsOptions}
                            </Select>
                        </FormItem>
                    </Col>
                    <Col xl={5} md={24}>
                        <FormItem>
                            <Select size="large" value={state.teacher} onChange={this.handleTeacherChange}>
                                {teacherOptions}
                            </Select>
                        </FormItem>
                    </Col>                        


                    <Col xl={12} md={24}>
                        <FormItem>
                            <InputNumber size="large" value={state.number} onChange={this.handleNumberChange} min={0} max={8} style={{ marginRight: '3%' }} />
                            <Select
                                size="large"
                                value={state.currency}                            
                                style={{ width: '32%', marginRight: '3%' }}
                                onChange={this.handleCurrencyChange}
                            >
                                <Option value="8th">8th</Option>
                                <Option value="6th">6th</Option>
                            </Select>
                        
                            {keys.length > 1 && index !== 0 ? (
                                <IconRemove
                                    type="minus-circle-o"
                                    disabled={keys.length === 1}
                                    onClick={onClickRemove}
                                />
                            ) : null}
                        </FormItem>
                    </Col>
                </Row> 
            </Fragment>
        );
    }
}
