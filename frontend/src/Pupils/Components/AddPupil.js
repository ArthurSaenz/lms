import React, { Component } from 'react';
import { Button, Modal, Form, DatePicker, Input, Select, Cascader, Col, Row, InputNumber } from 'antd';


const FormItem = Form.Item;
const MonthPicker = DatePicker.MonthPicker;
const Option = Select.Option;


const dataOfTeachers = [
  {
    value: "zhejiang",
    label: "Zhejiang",
    children: [
      {
        value: "hangzhou",
        label: "Hangzhou",
        children: [
          {
            value: "xihu",
            label: "West Lake"
          }
        ]
      }
    ]
  },
  {
    value: "jiangsu",
    label: "Jiangsu",
    children: [
      {
        value: "nanjing",
        label: "Nanjing",
        children: [
          {
            value: "zhonghuamen",
            label: "Zhong Hua Men"
          }
        ]
      }
    ]
  }
];

export const checkTypeUducationFunc = (rule, value, callback) => {
    if (value.currency === '8th') {
        if (value.number > 8) {
            callback('In 8th type of education maximum level is 8')
        }
        callback();
        return;
    } else {
        if (value.number > 6) {
            callback('In 6th type of education maximum level is 6')
        }
        if (value.number === 0) {
            callback('Level #0 must be in 8th type')
        }
        callback();
        return;
    }
};

 export class SixthType extends Component {
    constructor(props) {
        super(props);
        
        const value = this.props.value || {};
        this.state = {
            number: value.number || 1,
            currency: value.currency || '8th',
        };
    }

    componentWillReceiveProps(nextProps) {
        // Should be a controlled component.
        if ('value' in nextProps) {
            const value = nextProps.value;
            this.setState(value);
        }
    }

    
    handleNumberChange = (e) => {        
        const number = parseInt(e || 0, 10);
        if (isNaN(number)) {
            return;
        }
        if (!('value' in this.props)) {
            this.setState({ number });
        }
        this.triggerChange({ number });
    }


    handleCurrencyChange = (currency) => {
        if (!('value' in this.props)) {
            this.setState({ currency });
        }
        this.triggerChange({ currency });
    }

    triggerChange = (changedValue) => {
        // Should provide an event to pass value to Form.
        const onChange = this.props.onChange;
        if (onChange) {
            onChange(Object.assign({}, this.state, changedValue));
        }
    }
    render() {
        const { size } = this.props;
        const state = this.state;
        return (
            <span>
            <InputNumber value={state.number} onChange={this.handleNumberChange} min={0} max={8} style={{ marginRight: '3%' }} />
        {/* <Input
            type="text"
            size={size}
            value={state.number}
            onChange={this.handleNumberChange}
            style={{ width: '65%', marginRight: '3%' }}
        /> */}
        <Select
            value={state.currency}
            size={size}
            style={{ width: '32%' }}
            onChange={this.handleCurrencyChange}
        >
          <Option value="8th">8th</Option>
          <Option value="6th">6th</Option>
        </Select>
      </span>
        );
    }
}


const ModalFormAddPupil = Form.create()(
    (props) => {
        const { visible, onCancel, onCreate, form } = props;
        const { getFieldDecorator } = form;
        const checkTypeUducation = checkTypeUducationFunc;


        return (
            <Modal
                visible={visible}
                title="Add a new pupil"
                okText="Add"
                style={{ top: 20 }}
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <FormItem>
                        <Row gutter={16} style={{ height: '65px'}}>
                            <Col span={12}>
                                <FormItem label="Name">
                                    {getFieldDecorator('name', {
                                        rules: [{ required: true, message: 'Please input the title of collection!' }],
                                    })(
                                        <Input />
                                    )}
                                </FormItem>
                            </Col>
                            <Col span={12}>
                                <FormItem label='Surname'>
                                    {getFieldDecorator('surname', {
                                        rules: [{ required: true, message: 'Please input the title of collection!' }],
                                    })(
                                        <Input />
                                    )}
                                </FormItem>
                            </Col>
                        </Row>
                    </FormItem>

                    <FormItem>
                        <Row gutter={16} style={{ height: '65px'}}>
                            <Col span={12}>
                                <FormItem label="Birthday">
                                    {getFieldDecorator('date-picker', {
                                        rules: [{ type: 'object', required: true, message: 'Please select day!' }],
                                    })(
                                        <DatePicker showTime format="DD-MM-YYYY"/>
                                    )}
                                </FormItem>
                            </Col>
                            <Col span={12}>
                                <FormItem label="Number of phone">
                                    {getFieldDecorator('phone')(
                                        <Input />
                                    )}
                                </FormItem>
                            </Col>
                        </Row>
                    </FormItem>
                    <FormItem label="Basic school">
                        {getFieldDecorator('basic school')(<Input type="textarea" />)}
                    </FormItem>
                    <FormItem label="Living adress">
                        {getFieldDecorator('living')(<Input type="textarea" placeholder="Adress of living child" />)}
                    </FormItem>


                    <FormItem>
                        <Row gutter={16} style={{ height: '65px'}}>
                            <Col span={16}>
                                <FormItem label="Name and Surname of parent1">
                                    {getFieldDecorator('IdFarther')(<Input type="textarea" />)}
                                </FormItem>
                            </Col>
                            <Col span={8}>
                                <FormItem label="Number of phone1">
                                    {getFieldDecorator('phone1')(
                                        <Input />
                                    )}
                                </FormItem>
                            </Col>
                        </Row>

                    </FormItem>

                    <FormItem>
                        <Row gutter={16} style={{ height: '65px'}}>
                            <Col span={16}>
                                <FormItem label="Name and Surname of parent2">
                                    {getFieldDecorator('Idparent2')(<Input type="textarea" />)}
                                </FormItem>
                            </Col>
                            <Col span={8}>
                                <FormItem label="Number of phone2">
                                    {getFieldDecorator('phone2')(
                                        <Input />
                                    )}
                                </FormItem>
                            </Col>
                        </Row>

                    </FormItem>

                    
                    {/*<FormItem className="collection-create-form_last-form-item">*/}
                        {/*{getFieldDecorator('modifier', {*/}
                            {/*rules: [{ required: true, message: 'Please input the type of education' }],*/}
                            {/*initialValue: 'type'*/}
                        {/*})(*/}
                            {/*<Radio.Group>*/}
                                {/*<Radio value="6th">Public</Radio>*/}
                                {/*<Radio value="8th">Private</Radio>*/}
                            {/*</Radio.Group>*/}
                        {/*)}*/}
                    {/*</FormItem>*/}
                    <FormItem label="Select department and the teacher">
                        {getFieldDecorator ('teacher', {
                            rules: [{ type: 'array', message: 'Select the teacher and department'}]
                        })(
                            <Cascader options={dataOfTeachers}/>
                        )}
                    </FormItem>

                    <FormItem label="Start of education">
                        {getFieldDecorator('start-education', {
                            rules: [{ type: 'object', required: true, message: 'Please select month of start education!' }],
                        })(
                            <MonthPicker />
                        )}
                    </FormItem>
                    
                    <FormItem label="Level and type">
                        {getFieldDecorator('typeLevel', {
                            rules: [{ required: true, validator: checkTypeUducation}],
                        })(
                            <SixthType/>
                        )}
                    </FormItem>

                </Form>
            </Modal>
        );
    }
);

class AddPupilComponent extends React.Component {
    state = {
        visible: false,
    };

    showModal = () => {
        this.setState({ visible: true });
    }


    handleCancel = () => {
        this.setState({ visible: false });
    }

    handleCreate = () => {
        const form = this.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
    
            console.log('Received values of form: ', values);
            form.resetFields();
            this.setState({ visible: false });
        });
    }

    saveFormRef = (form) => {
        this.form = form;
    }


    render() {
        return (
            <div>
                <Button type="primary" onClick={this.showModal}>Add Pupil</Button>
                <ModalFormAddPupil
                    ref={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleSubmitForm}
                />
            </div>
        );
    }
}

export default AddPupilComponent;