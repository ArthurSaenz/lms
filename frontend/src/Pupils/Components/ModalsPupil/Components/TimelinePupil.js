import React from 'react';
import styled from 'styled-components';


const colorFucn = (a) => {
    switch(a) { 
        case "Main lesson": 
            return "#2eea71"; 
        case "Musical History": 
            return "#f7e81b"; 
        case "Theory of music": 
            return "#fe647e"; 
        case "Choir": 
            return "#50b4f2"; 
        case "ansemble": 
            return "#50b4f2";
        case "3rd lesson":
            return "#7F00FF";
        default:
            return " ";
    }
};


export const demo = {
    name: "Arthur",
    schedule: {
        Monday: [
            {
                name: "Main lesson", 
                time: ["16.20", "17.05"],
                teacher: "John",
                cabinet: 8,
                concertmaster: "Jack"
            },
            {
                name: "Theory of music",
                time: ["17.10", "18.25"],
                teacher: 'Smith',
                cabinet: 13,
                idClass: "5.6B"
            },
        ],

        Friday: [
            {
                name: "Main lesson",
                time: ["15.30", "16.15"],
                teacher: "John",
                cabinet: 8,
                concertmaster: "Jack",                
            },

            {
                name: "Musical History",
                time: ["17.10", "18.25"],
                teacher: 'Smith',
                cabinet: 13,
                idClass: "5.6B"
            },
        ],

        Saturday: [
            {
                name: "Choir",
                time: ["11:15", "12.05"],
                teacher: "Kate",
                cabinet: 8,
                concertmaster: "William"
            },
        ]
    },
    scheduleIds: ["Monday", "Friday", "Saturday"] 
};


const Wrapper = styled.div`
    padding: 0 1em;
`;


const Title = styled.h3`
    font-size: 1.4em;
    margin-bottom: .7em;
`;


const Timeline = styled.ul`
    list-style: none;
    margin: 0;
    padding: 0;
`;


const LiItem = styled.li`
    display: flex;
    flex-direction: row;
    -webkit-flex-direction: row;
    border-top: 1px solid #D2D4D5;
    padding: 1.5em 1em; 
`;


const DayWeek = styled.div`
    width: 20%;
    font-size: 1.3em;
`;


const Lessons = Timeline.extend`
    width: 80%;
`;


const LessonOne = styled.li`
    display: flex;
    flex-direction: row;
    padding-bottom: 1.7em;
    border: initial;
    
    p {
        margin: 1rem 0;
        line-height: 1.2em;
    }

    &:last-child {
        padding-bottom: 0;
    }
`;


// Timeline and Main-body
const Time = styled.div`
    font-size: 1.2em;
    padding-right: 1.5em;
`;


const Body = styled.div`
    display: flex;
    flex-direction: row;
    flex-grow: 1;
    justify-content: space-between;
    border-left: 1px solid #D2D4D5;      

    p:first-child {
        font-size: 1.2em;
        font-weight: 600;
    }

    &:hover {
        background-color: rgba(232, 232, 232, 0.5);
    }
`; 

const LessonDot = styled.div`
    width: 0.8em;
    height: 0.8em;
    border-radius: 100%;
    margin: 1.25em 1em 0 1.5em;
    background: ${props => colorFucn(props.colorLesson)}

`; 


const NameLesson = styled.div`
    flex-grow: 7;
`;


const NameTeacher = styled.div`
    flex-grow: 2;
    padding-right: 1.5em;
    p {
        text-align: right;
    }
`;


export const TimelineComponent = (props) => { 
    
    const data = props.data;

    return (
        <Wrapper>
            <Title>Class Time</Title> 
            <Timeline>
                { [...Array(data.scheduleIds.length).keys()].map((number) => 
                            <LiItem key={number}>
                                <DayWeek>
                                    {data.scheduleIds[number]}
                                </DayWeek>                   
                                <Lessons>
                                    {data.schedule[data.scheduleIds[number]].map((item, number1) => (
                                        <LessonOne key={number1}>
                                            <Time>
                                                <p>{item.time[0]}</p>
                                                <p>{item.time[1]}</p>
                                            </Time>
                                            <Body>
                                                <LessonDot colorLesson={item.name}/>
                                                <NameLesson>
                                                    <p>{item.name}</p>
                                                    <p>Classroom №{item.cabinet}</p>
                                                </NameLesson>

                                                <NameTeacher>
                                                    <p>{item.teacher}</p>
                                                    <p>{item.concertmaster}</p>
                                                </NameTeacher>
                                                    

                                            </Body>

                                        </LessonOne>
                                    ))}
                                </Lessons>
                            </LiItem>   
                        )        
                    }
            </Timeline>
        </Wrapper>
    )
};



