import React from 'react';
import { Modal, DatePicker, Form } from 'antd';
import { hideModal, destroyModal, mapModalProps, testAction } from '../../../../actions/modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


const FormItem = Form.Item;
const MonthPicker = DatePicker.MonthPicker;


const CustomizedForm = Form.create({
    onFieldsChange(props, changedFields) {
        props.onChange(changedFields);
    },


    mapPropsToFields(props) {
        return {
            month: Form.createFormField({
                ...props.month,
                value: props.month.value,
            }),
        };
    },


    onValuesChange(_, values) {
        console.log(values);
    },


})((props) => {
    const { getFieldDecorator } = props.form;
    return (
        <Form layout="inline">
            <FormItem label="Month of excluding">
                {getFieldDecorator('month', {
                    initialValue: null,
                    rules: [{ required: true, message: 'Month of excluding is required!' }],
                })(<MonthPicker/>)}
            </FormItem>
        </Form>
    );
});



export const DeleteModal = (props) => {
    console.log('props Modal ', props)
    const { dispatch, onOk, visible, month  } = props;
    const onChangeForm = changedFields => dispatch(mapModalProps({month: changedFields}));

    return (
        <Modal
            // title={`Delete pupil(s) ${props.selectedRows.length}`}
            title='Test modal'
            onOk={() => dispatch(testAction())}
            visible={visible}
            okButtonProps={{ disabled: (month ? false : true) }}         
            onCancel={() => dispatch(hideModal())}
            afterClose={() => dispatch(destroyModal())}
        >
            <CustomizedForm {...month} onChange={onChangeForm} />
        </Modal>
    )
};


CustomizedForm.defaultProps = {
    month: { value: null } 
};


const mapStateToProps = (state, ownProps) => ({
    // post: state.postsById[ownProps.postId]
    ...ownProps
})


export default connect(mapStateToProps)(DeleteModal);