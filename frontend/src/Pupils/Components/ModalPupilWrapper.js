import React, { Component } from 'react';
import {Col, Input, Form, Row, Modal, Button, Cascader, Tabs, Switch, Rate, DatePicker, Upload, Icon} from 'antd';
import { SixthType, checkTypeUducationFunc } from './AddPupil';
import { TimelineComponent, demo } from './ModalsPupil/Components/TimelinePupil';


const TabPane = Tabs.TabPane;
const FormItem = Form.Item;


const dataOfTeachers = [
  {
    value: "zhejiang",
    label: "Zhejiang",
    children: [
      {
        value: "hangzhou",
        label: "Hangzhou",
        children: [
          {
            value: "xihu",
            label: "West Lake"
          }
        ]
      }
    ]
  },
  {
    value: "jiangsu",
    label: "Jiangsu",
    children: [
      {
        value: "nanjing",
        label: "Nanjing",
        children: [
          {
            value: "zhonghuamen",
            label: "Zhong Hua Men"
          }
        ]
      }
    ]
  }
];


const FormEditPupil = props => {
    const handleSubmit = e => {
        e.preventDefault();
        form.validateFields((err, values) => {
            if (!err) {
                console.log("Received values of form: ", values);
            }
        });
    };


    const normFile = e => {
        console.log("Upload event:", e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

   
    const { form, data, visible } = props;
    const { getFieldDecorator } = form;


    // side-effect, must be in componentWillMount
    if (props.visible == false) {
        form.resetFields();
    }

    const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 14 }
    };

    return (
        <Form onSubmit={handleSubmit} className='edit-pupil'>
            <FormItem {...formItemLayout} label="Surname and Name">
                <Row gutter={16}>
                    <Col span={12}>
                        <FormItem>
                            {getFieldDecorator("name", {
                                initialValue: data.name,
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            "Please input the title of collection!"
                                    }
                                ]
                            })(<Input />)}
                        </FormItem>
                    </Col>
                    <Col span={12}>
                        <FormItem>
                            {getFieldDecorator("surname", {
                                initialValue: data.surName,
                                rules: [
                                    {
                                        required: true,
                                        message:
                                            "Please input the title of collection!"
                                    }
                                ]
                            })(<Input />)}
                        </FormItem>
                    </Col>
                </Row>
            </FormItem>

            <FormItem {...formItemLayout} label="Department and teacher">
                {getFieldDecorator("teacher", {
                    // initialValue: [data.department, data.teacher],
                    initialValue: [data.department, data.teacher, data.instruments[0]],
                    rules: [
                        {
                            type: "array",
                            message: "Select the teacher and department"
                        }
                    ]
                })(<Cascader options={props.dataDepartmentsTeachers } />)}
            </FormItem>
            
            <FormItem {...formItemLayout} label="Level and type">
                {getFieldDecorator('typeLevel', {
                    initialValue: {number: data.level, currency: ( data.sixth == 8 ? '8th' : '6th')},
                    rules: [{ required: true, validator: checkTypeUducationFunc}],
                })(
                    <SixthType/>
                )}
            </FormItem>

            <FormItem {...formItemLayout} label="Switch">
                {getFieldDecorator('switch', { valuePropName: 'checked' })(
                    <Switch />
                )}
            </FormItem>

            <FormItem {...formItemLayout} label="Rate">
                {getFieldDecorator('rate', {
                    initialValue: 3.5,
                })(
                    <Rate />
                )}
            </FormItem>
            
            <h4>Detail information:</h4>

            <FormItem {...formItemLayout} label="Number of phone">
                {getFieldDecorator('phone')(
                    <Input />
                )}
            </FormItem>                    

            <FormItem {...formItemLayout} label="Birthday">
                {getFieldDecorator('date-picker', {
                    rules: [{ type: 'object', required: true, message: 'Please select day!' }],
                })(
                    <DatePicker format="DD-MM-YYYY"/>
                )}
            </FormItem>               

            <FormItem {...formItemLayout} label="Basic school">
                {getFieldDecorator('basic school')(<Input type="textarea" />)}
            </FormItem>

            <FormItem {...formItemLayout} label="Living adress">
                {getFieldDecorator('living')(<Input type="textarea" placeholder="Adress of living child" />)}
            </FormItem>


            <FormItem {...formItemLayout} label="Parent1 and phone">
                <Row gutter={16}>
                    <Col span={16}>
                        <FormItem>
                            {getFieldDecorator('IdFarther')(<Input type="textarea" />)}
                        </FormItem>
                    </Col>
                    <Col span={8}>
                        <FormItem>
                            {getFieldDecorator('phone1')(
                                <Input />
                            )}
                        </FormItem>
                    </Col>
                </Row>
            </FormItem>

            <FormItem {...formItemLayout}  label="Parent1 and phone">
                <Row gutter={16}>
                    <Col span={16}>
                        <FormItem>
                            {getFieldDecorator('Idparent2')(<Input type="textarea" />)}
                        </FormItem>
                    </Col>
                    <Col span={8}>
                        <FormItem>
                            {getFieldDecorator('phone2')(
                                <Input />
                            )}
                        </FormItem>
                    </Col>
                </Row>
            </FormItem>

            <FormItem {...formItemLayout} label="Dragger">
                <div className="dropbox">
                    {getFieldDecorator('dragger', {
                        valuePropName: 'fileList',
                        getValueFromEvent: this.normFile,
                    })(
                        <Upload.Dragger name="files" action="/upload.do">
                            <p className="ant-upload-drag-icon">
                                <Icon type="inbox" />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">Support for a single or bulk upload.</p>
                        </Upload.Dragger>
                    )}
                </div>
            </FormItem>

            <FormItem wrapperCol={{ span: 12, offset: 6 }}>
                <Button type="primary" htmlType="submit">Submit</Button>
            </FormItem>
        </Form>
    );
};


const FormEditPupilWrapper = Form.create()(FormEditPupil);


class ModalCoreComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeKey: '1'
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.visibleState !== nextProps.visibleState) {
            this.setState({activeKey: '1'});
        }
    }

    onChange = (activeKey) => {
        this.setState({ activeKey });
    }

    render() {
        console.log('Tabs rendered!');
        return (
            <Tabs tabPosition="left" activeKey={this.state.activeKey} onChange={this.onChange}>
                <TabPane tab="Schedule" key='1'>
                    <TimelineComponent data={demo}/>
                </TabPane>
                <TabPane tab="Edit Data" key='2'>
                    <FormEditPupilWrapper data={this.props.data} dataDepartmentsTeachers={this.props.dataDepartmentsTeachers} visible={this.props.visibleState}/>
                </TabPane>
            {/* <TabPane tab="Tab 3" key="3">Content of Tab 3</TabPane> */}
            </Tabs>
        );
    }
}


export const ModalPupilWrapper = (props) => {   
    return (
        <Modal
            visible={props.visibleState}
            style={{ top: 20 }} 
            width="950px" 
            confirmLoading={props.confirmLoading} 
            modalText="Content of the modal"
            title={ props.visibleState ? (props.dataPupil.surName + ' ' + props.dataPupil.name) : null } 
            footer={null}                
            onCancel={props.handleCancel}
            // onOk={handleEditLocal}
            // okText="Create"
        >
            <ModalCoreComponent
                visibleState={props.visibleState}
                dataDepartmentsTeachers={props.dataDepartmentsTeachers}
                data={props.dataPupil}
            />
        </Modal>
    )
}



// unusage component
class EditableCell extends Component {
    constructor(props) {
        super(props);        
        this.state = {
            value: this.props.value,
            editable: false,
        };
        this.check = this.check.bind(this);
        this.handleChange = this.handleChange.bind(this);  
        this.edit = this.edit.bind(this);      
    }

    handleChange = (e) => {
        const value = e.target.value;
        this.setState({ value });
    }
    check = () => {
        this.setState({ editable: false });
        if (this.props.onChange) {
            this.props.onChange(this.state.value);
        }
    }

    edit = () => {
        this.setState({ editable: true });

    }
    render() {
        const { value, editable } = this.state;
        return (
            <div className="editable-cell">
                {
                    editable ?
                        <div className="editable-cell-input-wrapper">
                            <Input
                                value={value}
                                onChange={this.handleChange}
                                onPressEnter={this.check}
                            />
                            <Icon
                                type="check"
                                className="editable-cell-icon-check"
                                onClick={this.check}
                            />
                        </div>
                        :
                        <div className="editable-cell-text-wrapper">
                            {value || ' '}
                            <Icon
                                type="edit"
                                className="editable-cell-icon"
                                onClick={this.edit}
                            />
                        </div>
                }
            </div>
        );
    }
}

