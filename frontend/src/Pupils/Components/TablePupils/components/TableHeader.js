import React from 'react';
import {Menu, Dropdown, Button, Icon, Modal } from 'antd';
import styled from 'styled-components';
import { DELETE_PUPIL } from '../../../../constants/modal';


const confirm = Modal.confirm;

const exportData = e => console.log('export', e);


const SpanText = styled.span`
    margin: 0 .5em;
`;


const TableHeaderWrap = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 1em;

    button {
        margin-right: .5em;
        margin-bottom: .7em;
    }
`;


const ListDeletePupils = props => (props.pupils.map(el => <h3>el</h3>));


const showDeleteConfirm = props => {
    confirm({
        title: 'Are you sure delete this task?',
        content: <ListDeletePupils pupils={props.selectedPupils} />,
        okText: 'Delete',
        okType: 'danger',
        cancelText: 'No',
        onOk() {
            props.deletePupils()
            console.log('OK');
        },
        onCancel() {
            console.log('Cancel');
        },
    });
}


const showExcludeConfirm = props => {
    props.showModal(DELETE_PUPIL, { test: props.selectedPupils } )
};


const DropdownBtn = props => {

    const handleMenuClick = e => {
        switch (e.key) {
            case '2':
                showExcludeConfirm(props)
                return;
            case '3':
                showDeleteConfirm(props)
                return;
            default:
                return null;
        }
    };

    const menu = (
        <Menu onClick={handleMenuClick}>
            {/* <Menu.Item key="1">1st item</Menu.Item> */}
            <Menu.Item key="2">Exclude pupil(s)</Menu.Item>
            <Menu.Item key="3">Delete pupil(s)</Menu.Item>
        </Menu>
    );

    return (
        <Dropdown overlay={menu} >
            <Button>
                Actions <Icon type="down" />
            </Button>
        </Dropdown>
    )
};


export const TableHeader = props => (
    <TableHeaderWrap>
        <div>
            <Button onClick={props.setNameSort}>Sort name</Button>
            <Button onClick={props.clearFilters}>Clear filters</Button>
            <Button onClick={props.clearAll}>Clear filters and sorters</Button>
        </div>
        <div>
            <SpanText>
                {props.hasSelected && `Selected ${props.selectedRowKeys.length} items`}
            </SpanText>
            <Button
                type="primary"
                onClick={exportData}                
                disabled={!props.hasSelected}
                loading={props.loading}
            >
                Export
            </Button>
            {props.hasSelected && 
                <DropdownBtn 
                    selectedPupils={props.selectedRowKeys} 
                    showModal={props.showModal} 
                    deletePupils={props.deletePupils} />}
        </div>
    </TableHeaderWrap>
);