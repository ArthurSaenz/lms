import React, { Component } from 'react';
import { Table, Button } from 'antd'
import 'antd/lib/table/style/index.css'; // ???
import ModalPupilComponent from '../ModalPupilContainer';
import { TableHeader } from './components/TableHeader';
import { PUPIL_MODAL } from '../../../constants/modal';



export default class TableAntD extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRowKeys: [],
            loading: false
        }
    };

    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter);
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        })
    };

    clearFilters = () => {
        this.setState({ filteredInfo: null })
    };

    clearAll = () => {
        this.setState({
            filteredInfo: null,
            sortedInfo: null,
        })
    };

    setNameSort = () => {
        this.setState({
            sortedInfo: {
                order: 'ascend',
                columnKey: 'name',
            },
        })
    };

    onSelectChange = (selectedRowKeys) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        this.setState({ selectedRowKeys });
    };

        
    render() {
        const marginLeftEight = {marginLeft: 8};
        // selected
        const { loading, selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };

        const hasSelected = selectedRowKeys.length > 0;

        // sort and filter
        let { sortedInfo, filteredInfo } = this.state;
        sortedInfo = sortedInfo || {};
        filteredInfo = filteredInfo || {};


        const columns = [{
                title: 'Name',
                dataIndex: 'name',
                sorter: (a, b) => {
                    if (a.name < b.name) return -1;
                    if (a.name > b.name) return 1;
                    return 0;
                },
                sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
                // render:  (text, record) => <a onClick={() => this.props.handleModal(record.key)}>{text}</a>,
                render: (text, record) => <a onClick={ () => this.props.showModal(PUPIL_MODAL, { id: record.key }) }>{text}</a>
            }, {
                title: 'Teacher',
                dataIndex: 'teacher',
                sorter: (a, b) => {
                    if (a.teacher < b.teacher) return -1;
                    if (a.teacher > b.teacher) return 1;
                    return 0;
                },
                sortOrder: sortedInfo.columnKey === 'teacher' && sortedInfo.order
            }, {
                title: 'Department',
                dataIndex: 'department',
                filters: [
                    { text: 'String', value: 'String'},
                    { text: 'Piano', value: 'Piano' },
                    { text: 'Theory', value: 'Theory'},
                    { text: 'Orchestral', value: 'Orchestral'}],
                filteredValue: filteredInfo.department || null,
                onFilter: (value, record) => record.department.indexOf(value) === 0,
                sorter: (a, b) => {
                    if (a.department < b.department) return -1;
                    if (a.department > b.department) return 1;
                    return 0;
                },
                sortOrder: sortedInfo.columnKey === 'department' && sortedInfo.order
            }, {
                title: 'Level',
                dataIndex: 'level',
                width: '15%',
                filters: [{
                    text: '1 level',
                    value: 1
                },{
                    text: '2 level',
                    value: 2
                },{
                    text: '3 level',
                    value: 3
                },{
                    text: '4 level',
                    value: 4
                },{
                    text: '5 level',
                    value: 5
                },{
                    text: '6 level',
                    value: 6
                },{
                    text: '7 level',
                    value: 7
                },{
                    text: '8 level',
                    value: 8
                }],
                filteredValue: filteredInfo.level || null,
                onFilter: (value, record) => record.level.toString().indexOf(value) === 0,
                sorter: (a, b) => a.level - b.level,
                sortOrder: sortedInfo.columnKey === 'level' && sortedInfo.order,
            },{
                title: 'Sixth',
                dataIndex: 'sixth',
                width: '15%',
                filters: [{
                    text: '6',
                    value: '6'
                },{
                    text: '8',
                    value: '8'
                }],
                filterMultiple: false,
                filteredValue: filteredInfo.sixth || null,
                onFilter: (value, record) => record.sixth.toString().indexOf(value) === 0,
                sorter: (a, b) => a.sixth - b.sixth,
                sortOrder: sortedInfo.columnKey === 'sixth' && sortedInfo.order
            }];

        console.log('RENDER TABLE CONTAINER');
        
        return (
            <div>
                <TableHeader 
                    selectedRowKeys={selectedRowKeys} 
                    loading={loading} 
                    hasSelected={hasSelected}                    
                    setNameSort={this.setNameSort}
                    clearFilters={this.clearFilters}
                    clearAll={this.clearAll}
                    showModal={this.props.showModal}
                    deletePupils={this.props.deletePupils}
                />
                <Table columns={columns} rowSelection={rowSelection} dataSource={this.props.data} onChange={this.handleChange} />
                {/* <ModalPupilComponent/> */}
                {/* <ModalRootComponent/> */}
            </div>

        )
    };
}




