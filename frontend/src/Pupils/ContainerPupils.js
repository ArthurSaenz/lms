import React from 'react';
import TableAntD from './Components/TablePupils/TablePupilsContainer';
import { connect } from 'react-redux';
import { showModalPupil, showModal, testAction } from '../actions/modal';
import { getPupilsItems } from '../Pupils/Selectors/PupilsSelectors';
import { bindActionCreators } from 'redux';
import { Tabs } from 'antd';


const TabPane = Tabs.TabPane;

const styleTabBar = { marginBottom: '2em' }

const PupilsContainer = props => {

    const callback = key => console.log(key);

    return (
        <Tabs 
            defaultActiveKey="1" 
            onChange={callback} 
            animated={false}
            tabBarStyle={styleTabBar}    
        >
            <TabPane tab="Pupils" key="1"><TableAntD {...props} /></TabPane>
            <TabPane tab="Customers" key="2">Content of Tab Pane 2</TabPane>            
        </Tabs>
    )
};

// TEST SELECTOR CAUSE TWICE RENDERING

function mapStateToProps (state) {
    return {
        data: getPupilsItems(state)
    };
}


function mapDispatchToProps(dispatch) {
    return {
        handleModal: bindActionCreators(showModalPupil, dispatch),
        showModal: bindActionCreators(showModal, dispatch),
        deletePupils: bindActionCreators(testAction, dispatch)
         
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(PupilsContainer);