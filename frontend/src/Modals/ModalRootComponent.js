import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { DELETE_PUPIL, PUPIL_MODAL } from '../constants/modal';
import { hideModal } from '../actions/modal';
import { bindActionCreators } from 'redux';
import ModalPupilWrapper from '../Pupils/Components/ModalPupilContainer';
import DeleteModal from '../Pupils/Components/ModalsPupil/Components/DeleteModal';


const Modal = ({modalType, modalProps}) => {
    console.log('ModalRoot Props ', modalProps)
    switch (modalType) {
        case DELETE_PUPIL:
            return <DeleteModal {...modalProps} />;
        case PUPIL_MODAL:
            return <ModalPupilWrapper {...modalProps} />
        default:
            return null;
    }
}


const mapStateToProps = state => state.modal;


export const ModalRootComponent = connect(mapStateToProps)(Modal);





// Using Portal

// const modalRoot = document.getElementById('modal-root');


// const MODAL_COMPONENTS = {
//     [DELETE_PUPIL]: DeleteModal
//     // 'EXCLUDE_PUPIL': 
// };


// class Modal extends Component {
//     constructor(props) {
//         super(props);
//         this.el = document.createElement('div');
//     }

//     componentDidMount() {
//         modalRoot.appendChild(this.el);
//     }

//     componentWillUnmount() { 
//         modalRoot.removeChild(this.el);
//     }

//     render() {
//         const { modalType, modalProps } = this.props;
        
//         if (!modalType) {
//             return null
//         };

//         const ModalComponent = MODAL_COMPONENTS[modalType];

//         return ReactDOM.createPortal(
//             // this.props.children,
//             <ModalComponent {...modalProps}/>,
//             // <DeleteModal/>,          
//             this.el
//         );
//     }
// }


// const Modal = ({modalType, modalProps}) => {
//     if (!modalType) {
//         return null
//     };

//     const ModalComponent = DeleteModal;

//     return <ModalComponent {...modalProps} />

// };