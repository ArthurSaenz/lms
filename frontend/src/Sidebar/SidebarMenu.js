import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Layout, Icon } from 'antd';
import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

const Sider = Layout.Sider;
const { SubMenu } = Menu;
const MenuItemGroup = Menu.ItemGroup;


const SiderWrap = styled(Sider)`
    &&& {
        position: fixed;
        left: 0;
        z-index: 100;
        height: 100vh;
        overflow: auto;
        margin-top: 0;
    };


    @media (max-width: 992px) {
        &&& {

            margin-top: 50px; 
        }

    }
`;


const MenuItem = styled(Menu.Item)`
    @media (max-width: 992px) {
        &[style] {
            padding-left: 41% !important;
        }
    }
`;


const SubMenuItem = styled(Menu.Item)`
    @media (max-width: 992px) {
        text-align: center;

        &[style] {
            padding-left: 1em !important;
        }
    }
`;


const SpanMenuTitle = styled.span`
    @media (max-width: 992px) {
        padding-left: 41% !important;
    }
`;


const SidebarMenu =  (props) => (
    <SiderWrap        
        trigger={null}
        // collapsible
        collapsedWidth={props.isBreakpoint ? 0 : 80}
        width={props.isBreakpoint ? 400 : 200}
        breakpoint="lg"
        collapsed={props.collapsed}
        onBreakpoint={broken => props.toggleIsMobile(broken)}
        onCollapse={props.toggleSidebar}
    >
        <div className="logo"/>
        <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={['1']}
            selectedKeys={props.activeKey}
            onClick={item => props.toogleOpenKey(item.key)}         
            onSelect={ props.isBreakpoint ? props.toggleSidebar : undefined}
            defaultOpenKeys={['sub1']}            
        >
            <MenuItem key="1">
                <Link to='/'>
                    <Icon type="pie-chart" />
                    <span>Home</span>
                </Link>                    
            </MenuItem>
            <MenuItem key="3">
                <Link to='/teachers'>
                    <Icon type="desktop" />
                    <span>Teachers</span>
                </Link>
            </MenuItem>
            <MenuItem key="4">
                <Link to='/departments'>
                    <Icon type="desktop" />
                    <span>Departments</span>
                </Link>
            </MenuItem>
            <SubMenu key="sub1" title={<SpanMenuTitle><Icon type="user" /><span>Pupils</span></SpanMenuTitle>}>
                <MenuItemGroup key='g1'>
                    <SubMenuItem key="g11">
                        <Link to='/pupils'>
                            <span>Table of pupils</span>
                        </Link>                    
                    </SubMenuItem>
                    <SubMenuItem key="g12">
                        <Link to='/addpupil'>
                            Add new pupil
                        </Link>    
                    </SubMenuItem>
                    <SubMenuItem key="g13">
                        <Link to='/pupils-statistics'>                        
                            Statistics
                        </Link>
                    </SubMenuItem>
                </MenuItemGroup>
            </SubMenu>
        </Menu>
    </SiderWrap>
);


export default SidebarMenu;