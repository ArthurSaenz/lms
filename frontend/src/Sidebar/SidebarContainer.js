import React, { Component } from 'react';
import { connect } from 'react-redux';
import SidebarMenu from './Components/SidebarMenu';


function mapsStateToProps(state) {
    return {
        collapsed: state.headerSidebar.collapsed
    };
}


export default connect(mapsStateToProps)(SidebarMenu);