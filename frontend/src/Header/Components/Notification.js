import React from 'react';
import { Badge, Icon, Menu, Dropdown, Tag } from 'antd';
import styled from 'styled-components';


const Wrap = styled.div`
    margin: .3em 2em 0 0;
`;


const DropdownHeaderMenu = (props) => (
    <Wrap>
        <Dropdown trigger={['click']} overlay={<DropdownNotification name={props.name} number={props.number}/>}>
            <a className="ant-dropdown-link">
                <Badge count={props.number} style={{fontSize: '1.1em'}}>
                    <Icon type={props.nameIcon} style={{ fontSize: '1.6rem' }} />
                </Badge>                
            </a>
        </Dropdown>
    </Wrap>
);


const DropdownNotification = (props) => (

        <ul className="dropdown-header dropdown-menu-media" >
            <li className="dropdown-menu-header">
                <span style={{'lineHeight': '150%'}}>
                    {props.name}
                </span>
                {props.number > 0 &&
                    <span>
                        <Tag color="#f50">Новых: {props.number}</Tag>
                        
                    </span>
                }
            </li>
            <li>
                
            </li>
            <li className="dropdown-menu-footer">
                <a href="#">Відкрити меню повідомлень</a>
            </li>
        </ul>   
);




export default DropdownHeaderMenu;
