import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Icon } from 'antd';
import SearchInput from './SearchInput';
import Search from './Search';
import DropdownHeaderMenu from "./Notification";
import styled from 'styled-components';
import { AvatarComponent } from './Avatar';


const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;


const name1 = 'John Doe';


const IconWrap = styled(Icon)`
    font-size: 18px;
    padding: 0 24px;
    cursor: pointer;
    transition: color .3s;

    &:hover {
        color: #1890ff;
    }
`;


const HeaderWrap = styled.div`
    display: flex;
    width: ${props => props.collapsed ? 'calc(100% - 80px)' : 'calc(100% - 200px)'};
    margin-left: ${props => props.collapsed ? '80px' : '200px'};
    align-items: center;  
    justify-content: space-between;
    position: fixed;
    top: 0;
    left: 0;
    height: 50px;
    max-width: 100vw;
    z-index: 1000;  
    padding: 0 1.5em;
    background: #FFF;
    box-shadow: 2px 4px 20px -4px rgba(0,0,0,.1);
    transition: all 0.2s;  

    @media (max-width: 992px) {        
        width: 100%;
        padding: 0;
        margin-left: 0;        

    }

`;


const RightBlock = styled.div`
    display: flex;
    flex: 1;
    justify-content: flex-end;
`;





const Header = (props) => (    
    <HeaderWrap collapsed={props.collapsed}>

        <IconWrap
            type={props.collapsed ? 'menu-unfold' : 'menu-fold'}
            onClick={props.toggleSidebar}
        />
        {/* <SearchInput/> */}
        <Search/>
        <RightBlock>
            <DropdownHeaderMenu number={props.headerData.numberNotification} color='red' nameIcon="mail" name="Notification" />
            <DropdownHeaderMenu number={props.headerData.numberMessage} color='orange' nameIcon="notification" name="Mail" />
            <AvatarComponent name={name1}/>
        </RightBlock>
    </HeaderWrap>
);


Header.propTypes = {
    collapsed: PropTypes.bool.isRequired,
    toggleSidebar: PropTypes.func.isRequired,
    headerData: PropTypes.shape({
        numberMessage: PropTypes.number.isRequired,
        numberMessage: PropTypes.number.isRequired
    })
};


export default Header;