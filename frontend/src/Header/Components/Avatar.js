import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Dropdown, Menu, Icon } from 'antd';
import styled, { css } from 'styled-components';


const AvatarWrap = styled(Avatar)`
    &&& {
        margin-right: .3em;
        background-color: #87d068;
    }
`;


const IconWrap = styled(Icon)`
    &&& {
        margin-left: .3em;
    }
`;


const BeforeDropdownHeaderStyle = css`
    display: block;
    position: absolute;
    pointer-events: none;
    content: '';
    width: .7em;
    height: .7em;
    margin: 0;
    transform: rotate(45deg);
    background: #FFF;
    box-shadow: -1px -1px 0 0 rgba(34,36,38,.15);
`;


const MenuWrapper = styled(Menu)`  
    &&& {
        margin: 1.3em 0 0 0;    
    }

    &:before {
        top: -.3em;
        right: calc(50% - .5em);
        z-index: -1;
        ${BeforeDropdownHeaderStyle}
    }
`;

const LiMenu = styled(Menu.Item)`
    &&& {
        padding: .4em 2em;
    }    
`;


const menu = (
    <MenuWrapper>
        <LiMenu>
            <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">1st menu item</a>
        </LiMenu>
        <LiMenu>
            <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">2nd menu item</a>
        </LiMenu>
        <Menu.Divider />
        <LiMenu>
            <a>Exit</a>
        </LiMenu>
    </MenuWrapper>
);


const AvatarLink = styled.a`
    margin-right: 1em;
    font-size: 1.1em;
    font-weight: 500;
    color: rgba(0, 0, 0, 0.65);

    &:hover {
        color: #868e96;
    }

    &:active {
        color: #868e96;
    }
`;


const AvatarComponent = ({avatarLink = '', name}) => (
    <Dropdown overlay={menu} trigger={['click']}>
        <AvatarLink>
            <AvatarWrap src={avatarLink} icon="user" />
            {name}
            <IconWrap type="down" />
        </AvatarLink>
    </Dropdown>

);


AvatarComponent.propTypes = {
    avatarLink: PropTypes.string,
    name: PropTypes.string.isRequired
};


export { AvatarComponent };
