import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Input, Icon, AutoComplete } from 'antd';
import styled from 'styled-components';

const SpanSearch = styled.span`
    cursor: pointer;

`;

const AutoCompleteWrap = styled(AutoComplete)`
    &&& {
        transition: width 0.3s, margin-left 0.3s;
        width: auto;
        background: transparent;
        border-radius: 0;

        input {
            border: 0;
            padding-left: 0;
            padding-right: 0;
            box-shadow: none !important;

            ${props => props.show ?
                "width: 210px; margin-right: .5em; " 
                :
                "width: 0; margin-right: 0;"            
            }
    
    
            &:hover, &:focus {
                border: 1px solid #40a9ff;
                padding: 0 .5em 0 .5em;
            }
        }
    }
`;


class Search extends PureComponent {
    static propTypes = {    
        onSearch: PropTypes.func,
        onPressEnter: PropTypes.func,
        defaultActiveFirstOption: PropTypes.bool,
        dataSource: PropTypes.array,
        defaultOpen: PropTypes.bool,
    };


    static defaultProps = {
        defaultActiveFirstOption: false,
        onPressEnter: () => { },
        onSearch: () => { },
        dataSource: [],
        defaultOpen: false,
    };


    constructor(props) {
        super(props);
        this.state = {
            isOpened: false,
            value: ''
        }
    };


    onKeyDown = e => {
        if (e.key === 'Enter') {
            this.handlePressEnter();
        }
    };


    handlePressEnter() {
        const { onPressEnter } = this.props;
        onPressEnter(this.state.value)
    };


    onChange = value => {
        this.setState({ value });
        // Another onChange
        // const { onChange } = this.props;
        // if (onChange) {
        //     onChange();
        // }
    };


    leaveSearch = () => {
        this.setState({
            isOpened: false,
            value: '',
        });
    };

    
    enterSearch = () => {
        this.setState({ isOpened: true }, () => {
            if (this.state.isOpened) {
                this.input.focus();
            }
        });
    };

    getInputRef = node => { this.input = node};

    render() {
        const { isOpened, value } = this.state;
        const {...restProps} = this.props;
        console.log('Search state ', this.state );
        return(
            <SpanSearch onClick={this.enterSearch}>
                
                <AutoCompleteWrap
                    show={isOpened}
                    {...restProps}
                    value={value}
                    onChange={this.onChange}
                >
                    <Input
                        placeholder=''
                        ref={this.getInputRef}
                        onBlur={this.leaveSearch}
                        onKeyDown={this.onKeyDown}
                    
                    />
                </AutoCompleteWrap>
                <Icon type="search" key="Icon" />
            </SpanSearch>
            
        )
    }
};

export default Search;
