import { createSelector } from 'reselect';

const getMessage = (state) => state.headerSidebar.message;
const getNotification = (state) => state.headerSidebar.notification;
const getNumberMessage = (state) => state.headerSidebar.numberMessage;
const getNumberNotification = (state) => state.headerSidebar.numberNotification;


export const getMessageNotificationData = createSelector(
    [ getMessage, getNotification, getNumberMessage, getNumberNotification ],
    (message, notification, numberSms, numberInfo) => ({ 
        messageData: message, 
        notificationData: notification, 
        numberMessage: numberSms, 
        numberNotification:  numberInfo})
);