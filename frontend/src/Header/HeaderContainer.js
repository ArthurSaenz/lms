import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from './Components/Header';
import { getMessageNotificationData } from './Selectors/Selectors';


function mapStateToProps(state, props) {    
    return {
        collapsed: props.collapsed,
        toggleSidebar: props.toggleSidebar,
        // headerData: getMessageNotificationData(state), 
        headerData: state.headerSidebar
    };
}


export default connect(mapStateToProps)(Header);