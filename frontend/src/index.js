import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import mainReducer from './reducers/mainReducer';
import registerServiceWorker from './registerServiceWorker';
import { createBrowserHistory } from 'history';
import { connectRouter, routerMiddleware, ConnectedRouter } from 'connected-react-router';
import thunk from 'redux-thunk';
import './app-assets/App.css';
import { fetchDefaultData } from './actions';


const history = createBrowserHistory();


const middlewares = [
    thunk,
    routerMiddleware(history)
];


// compose (before apply...) for add redux devtools
const store = createStore(
    connectRouter(history)(mainReducer), 
    compose(applyMiddleware(...middlewares), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() )    
);


console.log('State', store.getState());


// fetch initialState redux state
store.dispatch(fetchDefaultData()).then(() => console.log('fetched data ', store.getState() ))




ReactDOM.render(    
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>        
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();


//why-render

// if (process.env.NODE_ENV !== "production") {
//     const { whyDidYouUpdate } = require("why-did-you-update");
//     whyDidYouUpdate(React);
// }