import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin } from 'antd';
import * as actions from '../actions/departments';



class DepartmentPage extends Component {
    constructor(props) {
        super(props)
    };
    
    componentDidMount() {
        this.props.fetchDepartment(this.props.match.params.id)
    }

    render() {
        console.log('Department page', this.props)
        return (
            <Spin spinning={this.props.isLoading}>
                <h1>Department Page</h1>
            </Spin>            
        )
    }
};


const mapStateToProps = (state, ownProps) => {
    return {
        isLoading: state.departments.isLoading,
        department: state.departments.byId[ownProps.match.params.id]
    }
};


const mapDispatchToProps = dispatch => {
    return {
        fetchDepartment: bindActionCreators(actions.fetchDepartment, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DepartmentPage);



