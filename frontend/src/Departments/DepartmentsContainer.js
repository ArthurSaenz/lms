import React from 'react';
import { Card, Icon, Avatar } from 'antd';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import DepartmentPage from './Department';

const { Meta } = Card;

const testDepartments = [
    {
        title: 'Piano',
        description: 'This is the description',
        imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    }, {
        title: 'String',
        description: 'This is the description',
        imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    }, {
        title: 'Theory',
        description: 'This is the description',
        imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    },     {
        title: 'Piano',
        description: 'This is the description',
        imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    }, {
        title: 'String',
        description: 'This is the description',
        imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    }, {
        title: 'Theory',
        description: 'This is the description',
        imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
    }
];


const Wrap = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;

    @media (max-width: 992px) {
        flex-direction: column;
        flex-wrap: nowrap;
    }
`;


const CardWrap = styled(Card)`
    &&& {
        flex: 0 23em;
        margin: 1em;   

        &:hover {
            box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
        }
        
        @media (max-width: 992px) {
            flex: 1 0 20em;
        }
    }



`;


const CardsDepartments = props => {
    console.log('Cards ', props);
    return (
        <Wrap>
            {props.items.map((el, index) => (
                <CardWrap
                    key={index}
                    cover={<img alt="example" src={el.imgCoverSrc} />}
                    actions={[<Icon type="setting" />]}
                >
                    <Link to={'/departments/' + el.id}>
                        <Meta
                            avatar={<Avatar src={el.imgAvatarSrc}/>}
                            title={el.title}
                            description={el.description}
                        />
                    </Link>
                </CardWrap> 
            ))}
        </Wrap> 
    )
};



const DepartmentsContainer = props => {
    console.log('Test props departmentaaaaa ', props);
    return(
            <Switch>
                <Route exact path='/departments' render={ () => <CardsDepartments items={props.departments}/>}/>
                <Route path='/departments/:number' render={ (props) => <DepartmentPage {...props}/> }/>
            </Switch>
    )
};

const mapStateToProps = (state) => {
    return {
        // selector NEED!
        departments: Object.values(state.departments.byId)
    }
};


export default connect(mapStateToProps)(DepartmentsContainer); 