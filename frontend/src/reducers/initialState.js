// initial demo state
const data = {
    pupils: {
        byId: {
            1: {
                id: 1,
                name: "Alex",
                surName: 'West',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 4,
                level: 5,
                sixth: 6,
                instruments: [4],
                personalInfo: {
                    numberPhone: 637778891,
                    generalSchool: '#4',
                    birthday: '',
                    livingAddres: ''
                }
            },
            2: {
                id: 2,
                name: "Mark",
                surName: 'Paddington',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 3,
                level: 3,
                sixth: 8,
                instruments: [2],
                personalInfo: {
                    numberPhone: 637778892,
                    generalSchool: '#4',
                    birthday: '',
                    livingAddres: ''
                }
            },
            3: {
                id: 3,
                name: "John",
                surName: 'Benny',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 1,
                level: 1,
                sixth: 6,
                instruments: [3],
                personalInfo: {
                    numberPhone: 637778893,
                    generalSchool: '#4',
                    birthday: '',
                    livingAddres: ''
                }
            },
            4: {
                id: 4,
                name: "David",
                surName: 'Dennis',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 2,
                level: 2,
                sixth: 8,
                instruments: [4],
                personalInfo: {
                    numberPhone: 637778894,
                    generalSchool: '#4',
                    birthday: '',
                    livingAddres: ''
                }
            },
            5: {
                id: 5,
                name: "Adam",
                surName: 'Ben',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 2,
                level: 1,
                sixth: 8,
                instruments: [5],
                personalInfo: {
                    numberPhone: 637778895,
                    generalSchool: '#9',
                    birthday: '',
                    livingAddres: ''
                }
            },
            6: {
                id: 6,
                name: "Henry",
                surName: 'Brams',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 4,
                level: 5,
                sixth: 6,
                instruments: [4],
                personalInfo: {
                    numberPhone: 637778896,
                    generalSchool: '#17',
                    birthday: '',
                    livingAddres: ''
                }
            },
            7: {
                id: 7,
                name: "Carlos",
                surName: 'East',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 2,
                level: 2,
                sixth: 6,
                instruments: [3],
                personalInfo: {
                    numberPhone: 637778897,
                    generalSchool: '#31',
                    birthday: '',
                    livingAddres: ''
                }
            },
            8: {
                id: 8,
                name: "Andrew",
                surName: 'White',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 3,
                level: 3,
                sixth: 8,
                instruments: [2],
                personalInfo: {
                    numberPhone: 637778899,
                    generalSchool: '#14',
                    birthday: '',
                    livingAddres: ''
                }
            },
            9: {
                id: 9,
                name: "Jake",
                surName: 'Black',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 3,
                level: 2,
                sixth: 8,
                instruments: [3],
                personalInfo: {
                    numberPhone: 637778887,
                    generalSchool: '#22',
                    birthday: '',
                    livingAddres: ''
                }
            },
            10: {
                id: 10,
                name: "Benjamin",
                surName: 'Weed',
                lessons: {
                    1 : { day: 0, time: 0 },
                    2 : { day: 4, time: 2 },
                },
                main_lessons: [ 1, 2 ],
                teacher: 1,
                level: 4,
                sixth: 6,
                instruments: [3],
                personalInfo: {
                    numberPhone: 637778857,
                    generalSchool: '#24',
                    birthday: '',
                    livingAddres: ''
                }
            }
        },
        allIds: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        hasError: false,
        isLoading: false
    },

    teachers: {
        byId: {
            1: {
                id: 1,
                name: "Bob A.",
                department: 1,
                instruments: [1, 5],
                value: 23,
                pupils: [1, 2]
            },
            2: {
                id: 2,
                name: "Emily D.",
                department: 2,
                instruments: [2, 4, 3],
                value: 25,
                pupils: [3, 4]
            },
            3: {
                id: 3,
                name: "Johnson R.",
                department: 3,
                instruments: [3, 1, 2],
                value: 20,
                pupils: [5, 6, 7]
            },
            4: {
                id: 4,
                name: "Kate V.",
                department: 4,
                instruments: [4, 3],
                value: 18,
                pupils: [8, 9, 10]
            }
        },
        allIds: [1, 2, 3, 4],
        hasError: false,
        isLoading: false
    },

    departments: {
        byId: {
            1: {
                id: 1,
                title: "String",
                description: 'This is the description',
                imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
                imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
                teachers: [1],
                pupils: [1, 2],
                head: 3,
                instruments: [1, 5]
            },
            2: {
                id: 2,
                title: "Piano",
                description: 'This is the description',
                imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
                imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
                teachers: [2],
                pupils: [3, 4],
                head: 1,
                instruments: [2, 6]
            },
            3: {
                id: 3,
                title: "Theory",
                description: 'This is the description',
                imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
                imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
                teachers: [3],
                pupils: [5, 6, 7],
                head: 2,
                instruments: [3]
            },
            4: {
                id: 4,
                title: "Orchestral",
                description: 'This is the description',
                imgAvatarSrc: "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
                imgCoverSrc: "https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png",
                teachers: [4],
                pupils: [8, 9, 10],
                head: 3,
                instruments: [4]
            }
        },
        allIds: [1, 2, 3, 4],
        hasError: false,
        isLoading: false
    },

    instruments: {
        byId: {
            1: {
                name: 'Piano'
            },
            2: {
                name: 'Violin'
            },
            3: {
                name: 'Accordion'
            },
            4: {
                name: 'Guitar'
            },
            5: {
                name: 'Cello'
            },
            6: {
                name: 'Vocal'
            },
        },
        allIds: [1, 2, 3, 4, 5, 6],
        hasError: false,
        isLoading: false
    },

    addPupilForm: {
        valuesFields: {
            //step1
            nameChild: null,
            surnameChild: null,
            birthday: null,
            addressChild: null,
            numberChild: null,
            mainSchool: null,
            levelSchool: null,
            //step2
            nameMother: null,
            surnameMother: null,
            numberMother: null,
            emailMother: null,
            workMother: null,
            nameFarther: null,
            surnameFarther: null,
            numberFarther: null,
            emailFarther: null,
            workFarther: null,
            //step3
            teacher: null,
            fields: [],
            startEducation: null,
            comment: null,
            isInstrument: null
        },
        isLoadingRequest: false,
        isFailure: false,
    },

    header: {
        numberMessage: 1,
        numberNotification: 3,
        message: {},
        notification: {}
    },

    modal1: {
        modalType: null,
        modalProps: {},
        visible: false,
        confirmLoading: false
    },

    modal: {
        modalType: null,
        modalProps: {}
    }
};


export default data;