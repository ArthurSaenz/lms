import { combineReducers } from "redux";
import data from './initialState';
import * as el1 from "../constants/pupils";
import * as el2 from "../constants/teachers";
import * as el3 from "../constants/departments";
import * as el4 from "../constants/modal";
import * as el5 from "../constants/addpupilform";
import * as modal from '../constants/modal';


const departmentsReducer = (state = data.departments, action) => {
    switch (action.type) {
        case el3.DEPARTMENTS_ITEMS_IS_LOADING:
            return Object.assign({}, state, { isLoading: action.isLoading });
        case el3.DEPARTMENTS_ITEMS_HAS_ERROR:
            return Object.assign({}, state, { hasError: action.hasError });
        case el3.DEPARTMENTS_ITEMS_SUCCESS:
            return Object.assign({}, state, {
                byId: { ...state.byId, ...action.items.byId },
                allIds: [...state.allIds, action.items.allIds],
                isLoading: action.isLoading
            });
        case el3.DEPARTMENT_REMOVE:
                const newAllIds = state.allIds.filter(el => { el !== action.id });
                const newbyId = newAllIds.reduce((object, key) => object[key] = state.byId[key])
            return Object.assign({}, state, {
                byId: newbyId,
                allIds: newAllIds,
                isLoading: action.isLoading
            });
        default:
            return state;
    }
};


const instrumentsReducer = (state = data.instruments, action) => {
    switch (action.type) {
        default:
            return state;
    }
};


const pupilsReducer = (state = data.pupils, action) => {
    switch (action.type) {
        case el1.PUPILS_ITEMS_IS_LOADING:
            return Object.assign({}, state, { hasError: action.hasError });
        case el1.PUPILS_ITEMS_HAS_ERROR:
            return Object.assign({}, state, { isLoading: action.isLoading, hasError: action.hasError });
        case el1.PUPILS_ITEMS_SUCCESS:
            return Object.assign({}, state, {
                byId: { ...state.byId, ...action.items.byId },
                allIds: [...state.allIds, action.items.allIds],
                isLoading: action.isLoading
            });        
        default:
            return state;
    }
};


const teachersReducer = (state = data.teachers, action) => {
    switch (action.type) {
        case el2.TEACHERS_ITEMS_IS_LOADING:
            return Object.assign({}, state, { isLoading: action.isLoading });
        case el2.TEACHERS_ITEMS_HAS_ERROR:
            return Object.assign({}, state, { hasError: action.hasError });
        case el2.TEACHERS_ITEMS_SUCCESS:
            return Object.assign({}, state, {
                byId: { ...state.byId, ...action.items.byId },
                allIds: [...state.allIds, action.items.allIds],
                isLoading: action.isLoading
            });
        default:
            return state;
    }
};


const modalReducer1 = (state = data.modal, action) => {
    switch (action.type) {
        case el4.SHOW_MODAL:
            return Object.assign({}, state, {
                modalProps: action.modalProps,
                visible: action.visible
            });
        case el4.HIDE_MODAL:
            return Object.assign({}, state, {visible: false});
        default:
            return state;
    }
};


const modalReducer = (state = data.modal, action) => {
    switch (action.type) {
        case modal.SHOW_MODAL:
            return Object.assign({}, state, {
                modalType: action.modalType,
                modalProps: {...action.modalProps, visible: true}
            });
        case modal.HIDE_MODAL:
            return Object.assign({}, state, {                
                modalProps: {...state.modalProps, visible: false}
            })
        case modal.DESTROY_MODAL:
            return Object.assign({}, state, data.modal)
        case modal.MODAL_PROPS:
            return Object.assign({}, state, {modalProps: {...state.modalProps, ...action.modalProps }})
        default:
            return state
    }
}


const headerSidebarReducer = (state = data.header, action) => {
    switch (action.type) {
        default:
            return state;
    }
};


const addPupilFormReducer = (state = data.addPupilForm, action) => {
    switch (action.type) {
        case el5.STEP1_VALUE_SAVE:
            return Object.assign({}, state, {valuesFields: {...state.valuesFields, ...action.data}}); // BE CAREFUL, design state
        case el5.CLEAR_FIELDS_VALUE:
            return Object.assign({}, state, {valuesFields: {}});
        case el5.IS_LOADING_REQUEST:
            return Object.assign({}, state, {isLoadingRequest: true, isFailure: false});
        case el5.IS_FAILURE:
            return Object.assign({}, state, {isFailure: true, isLoadingRequest: false});
        default:
            return state;
    }
};


const mainReducer = combineReducers({
    pupils: pupilsReducer,
    teachers: teachersReducer,
    departments: departmentsReducer,
    instruments: instrumentsReducer,
    modal: modalReducer,
    headerSidebar: headerSidebarReducer,
    addPupilForm: addPupilFormReducer
});


export default mainReducer;
