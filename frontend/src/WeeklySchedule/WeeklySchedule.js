import React, { Component, Fragment } from 'react';
import ScheduleGrid from './Components/ScheduleGrid';
import ScheduleSidebar from './Components/SidebarWeek/ScheduleSidebar';
import HTML5Backend from 'react-dnd-html5-backend';
import { Button, Checkbox } from 'antd';
import { DragDropContext } from 'react-dnd';
import { Prompt } from 'react-router-dom';


const timeline = [
    {
        number: 1,
        timeStart: '8:50'
    },
    {
        number: 2,
        timeStart: '9:40'
    },
    {
        number: 3,
        timeStart: '10:30'
    },
    {
        number: 4,
        timeStart: '11:20'
    },
    {
        number: 5,
        timeStart: '12:10'
    },
    {
        number: 6,
        timeStart: '13:00'
    },
    {
        number: 7,
        timeStart: '13:50'
    },
    {
        number: 8,
        timeStart: '14:40'
    },
    {
        number: 9,
        timeStart: '15:30'
    },
    {
        number: 10,
        timeStart: '16:20'
    },
    {
        number: 11,
        timeStart: '17:10'
    },
    {
        number: 12,
        timeStart: '18:00'
    },
    {
        number: 13,
        timeStart: '18:50'
    },
    {
        number: 14,
        timeStart: '19:40'
    },
    
]


const demoData2 = [
    {
        ID: 36,
        1 : { day: 1, time: 8 },
        2 : { day: 2, time: 8 },
        lessons: [ 1, 2 ],
        level: 5,
        sixth: 8
    },  
    {
        ID: 37,
        1 : { day: 2, time: 9 },
        2 : { day: 3, time: 9 },
        lessons: [ 1, 2 ],
        level: 4,
        sixth: 6
    }, 
    {
        ID: 38,
        1 : { day: 3, time: 7 },
        2 : { day: 4, time: 7 },
        lessons: [ 1, 2 ],
        level: 3,
        sixth: 6
    },
    {
        ID: 39,
        1 : { day: null, time: null },
        2 : { day: 4, time: 2 },
        lessons: [ 1, 2 ],
        level: 5,
        sixth: 8
    },
    {
        ID: 40,
        1 : { day: null, time: null },
        2 : { day: null, time: null },
        lessons: [ 1, 2 ],
        level: 6,
        sixth: 8
    },
    {
        ID: 41,
        1 : { day: null, time: null },
        2 : { day: null, time: null },
        lessons: [ 1, 2 ],
        level: 3,
        sixth: 4
    },
    {
        ID: 42,
        1 : { day: null, time: null },
        2 : { day: null, time: null },
        lessons: [ 1, 2 ],
        level: 2,
        sixth: 8
    },
    {
        ID: 43,
        1 : { day: null, time: null },
        2 : { day: null, time: null },
        lessons: [ 1, 2 ],
        level: 6,
        sixth: 8
    },
    {
        ID: 44,
        1 : { day: null, time: null },
        2 : { day: null, time: null },
        lessons: [ 1, 2 ],
        level: 3,
        sixth: 6
    },
    {
        ID: 45,
        1 : { day: null, time: null },
        2 : { day: null, time: null },
        lessons: [ 1, 2 ],
        level: 1,
        sixth: 8
    },
    {
        ID: 46,
        1 : { day: null, time: null },
        2 : { day: null, time: null },
        lessons: [ 1, 2 ],
        level: 5,
        sixth: 6
    },
    {
        ID: 47,
        1 : { day: null, time: null },
        2 : { day: null, time: null },
        lessons: [ 1, 2 ],
        level: 8,
        sixth: 8
    },
]

const colorName =  ['aque', 'pink', 'yellow', 'blue', 'salat', 'green', 'orange', 'violet', 'red', 'aque', 'pink', 'yellow', 'blue', 'salat', 'green', 'orange', 'violet', 'red' ];

// Static Function

const data = (colorName, demoData) => (demoData.map((el, index) => ({...el, color: colorName[index] })));
// add func to maps color to pupil to mapStateToProps
const data1 = data(colorName, demoData2);

const ifMorningLesson = (array) => array.some(el => el.lessons.some(el2 => el[el2].time <= 5 && el[el2].time >= 1 ));


class WeeklyApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pupilsArray: data1,
            isChanged: false,
            morningLessons: ifMorningLesson(data1),
        };
    }


    isChangeFunc = (item) => {
        console.log("isChanged! ", item)
        if (this.state.isChanged !== item) {
            this.setState({isChanged: item})
        }; 
    }


    setFullGrid = () => {
        this.setState(prevState => ({ morningLessons: !prevState.morningLessons}))
    };


    moveItem = (idMovedItem, newPosition, numberLesson) => {        
        this.setState({            
            pupilsArray: this.state.pupilsArray.map(item => {
                if (item.ID == idMovedItem) {
                    this.isChangeFunc(true);
                    return Object.assign({},
                        item,
                        { [numberLesson]: {                            
                                day: newPosition.day,
                                time: newPosition.time
                            }                 
                    });
                }
                return item
            })
        })
    };


    clearAll = () => {
        this.setState({
            pupilsArray: this.state.pupilsArray.map( item => {
                item.lessons.forEach( lesson => item[lesson] = {day: null, time: null})
                return item
            }),
            isChanged: true
        })
    };


    // DONT WORK
    resetDefaultState = () => {
        console.log('RESET ');
        this.setState({
            pupilsArray: data1,
            isChanged: false
        })
    };


    render() {

        const disableButton = ifMorningLesson(this.state.pupilsArray);
        console.log('main state ',  this.state, disableButton);
        return (
            <Fragment>
                <div className="main-header-btns">
                    <div className="left-block">
                        
                    </div>
                    <div className="right-block">
                        <Checkbox disabled={disableButton} checked={this.state.morningLessons} onChange={this.setFullGrid}>Morning Lessons</Checkbox>
                        <Button onClick={this.clearAll}>Clear</Button>
                    </div>
                </div>
                <div className='weekly-container'>
                    <Prompt when={this.state.isChanged} message="Are you sure you want to leave?"/>
                    <ScheduleSidebar
                        pupilsArray={this.state.pupilsArray}
                        moveItem={this.moveItem}
                    />
                    <ScheduleGrid 
                        pupilsArray={this.state.pupilsArray}
                        moveItem={this.moveItem} 
                        columns={6}
                        timelineData={this.state.morningLessons ? timeline : timeline.slice(-9, -1)}
                    />                
                </div>
                <div className="main-footer-btns">
                    <div className="left-block">

                    </div>
                    <div className="right-block">
                        <Button onClick={this.resetDefaultState}>Reset</Button>
                        <Button type="primary">Save</Button>
                    </div>
                </div>
            </Fragment>          

        )
    }
}


export default DragDropContext(HTML5Backend)(WeeklyApp);