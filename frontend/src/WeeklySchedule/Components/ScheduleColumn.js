import React, { Component } from 'react';
import SubjectSection from './SubjectSection';


class ScheduleColumn extends Component {
    getSectionData = (x, y) => {
        const numb = {};
        numb.indexArray = this.props.pupilsArray.findIndex( item => {
            return item.lessons.some((el) => {if (item[el].day == x && item[el].time == y) {
                numb.indexLesson = el
                return true
            }})                
        });
        
        if (numb.indexArray == -1) {
            return false 
        };

        return Object.assign({}, this.props.pupilsArray[numb.indexArray], { numberLesson: numb.indexLesson })
    };

    generateSection = yPos => {
        const {xPos} = this.props;
        const sectionData = this.getSectionData(xPos, yPos);

        return (
            <div 
                className='cell-w'
                key={`${yPos}_${xPos}`}
            >
                <SubjectSection
                    yPos={yPos}
                    xPos={xPos}
                    sectionData={sectionData}
                    key={`${yPos}_${xPos}`}
                    moveItem={this.props.moveItem}
                />
            </div>
        )
    };


    render() {
    const { weekDay, itemsInColumn, xPos } = this.props;
    console.log('Rendered column #', xPos, itemsInColumn )
        return (
            <div
                className='column-w'                
            >
                <div className='cell-w day'>{weekDay}</div>
                    {itemsInColumn.map(el => this.generateSection(el.number))}
            </div>
        );
    }
}

export default ScheduleColumn;
