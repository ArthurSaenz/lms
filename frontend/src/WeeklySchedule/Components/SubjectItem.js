import React, { Component } from 'react';
import { DragSource } from 'react-dnd';
import { Button } from 'antd';
import { getEmptyImage } from 'react-dnd-html5-backend';


const subjectSource = {
    beginDrag(props, monitor, component) {
        return props;
    },

    endDrag(props, monitor, component) {
        if (!monitor.didDrop()) {
            return;
        }

        const item = monitor.getItem();
        const dropResult = monitor.getDropResult();
        
        props.moveItem(item.sectionData.ID, {
            day: dropResult.xPos,
            time: dropResult.yPos
        }, item.sectionData.numberLesson );
    }
};


function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging(),
        connectDragPreview: connect.dragPreview()
    };
}


class SubjectItem extends Component {

    componentDidMount() {
		// Use empty image as a drag preview so browsers don't draw it
		// and we can draw whatever we want on the custom drag layer instead.
		this.props.connectDragPreview(getEmptyImage(), {
			// IE fallback: specify that we'd rather screenshot the node
			// when it already knows it's being dragged so we can hide it with CSS.
			captureDraggingState: true,
		})
	}


    render() {
        const { connectDragSource, isDragging, sectionData, moveItem } = this.props;
      
        return connectDragSource(
            <div
                className={'cell-item ' + sectionData.color } 
                style={{ opacity: isDragging ? 0 : 1 }} 
            >
                <div>
                    <p>{'Hello ' + sectionData.ID}</p> 
                    <p>{sectionData.level + '('+ sectionData.sixth + ')'}</p>
                </div>
                
                <Button 
                    className='btn-weekly' 
                    shape="circle" 
                    icon="close" 
                    size='small' 
                    onClick={() => moveItem(sectionData.ID, { day: null, time: null }, sectionData.numberLesson )} />
            </div>
        );
    }
}



export default DragSource('subjectItem', subjectSource, collect)(SubjectItem);