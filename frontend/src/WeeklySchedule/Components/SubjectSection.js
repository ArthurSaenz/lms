import React, { PureComponent } from 'react';
import { DropTarget } from 'react-dnd';
import SubjectItem from './SubjectItem';


const SubjectSectionTarget = {
    drop(props) {
        return props;
    },
    canDrop(props) {
        // attention on can drop section
        return props.sectionData == false;
    }
};


const collect = (connect, monitor) => {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver() 
    }
};


class SubjectSection extends PureComponent {
    itemTemplate(xPos, yPos) {
        return (
            <div
                className='cell-box'
                key={'${xPos}_${yPos}'}
            >
                <SubjectItem 
                    key={this.props.sectionData.ID}
                    moveItem={this.props.moveItem}
                    // index={index}
                    sectionData={this.props.sectionData}
                />
            </div>
        )
    }


    emptyItemTemplate(xPos, yPos, isOver) {
        return (
            <div
                className={'cell-empty ' + ( isOver ? 'is-over' : '' ) }
                key={'${xPos}_${yPos}'}
            >
                <div className='emptyCell'></div>
            </div>
        )
    }


    render() {
        console.log('rendering section ')
        const { connectDropTarget, isOver, sectionData, xPos, yPos } = this.props;
        // const styles = isOver ? { opacity: 0.7 } : null;
        const sectionIsEmpty = sectionData == false;

        const subjectSection = 
            sectionIsEmpty 
                ? this.emptyItemTemplate(xPos, yPos, isOver)
                : this.itemTemplate(xPos, yPos, isOver);
        
        return connectDropTarget(subjectSection);
    }
}

export default DropTarget('subjectItem', SubjectSectionTarget, collect)(SubjectSection);