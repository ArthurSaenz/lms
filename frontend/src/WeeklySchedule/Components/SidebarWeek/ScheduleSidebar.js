import React from 'react';
import SidebarItems from './SidebarItems';



const ScheduleSidebar = (props) => {

    const sidebarData = props.pupilsArray.reduce( (filtered, item) => {
        item.lessons.some(el => {if (item[el].day == null && item[el].time == null) {
            let newValue = Object.assign({}, item, { numberLesson: el });
            filtered.push(newValue);
            return true
        }});
    
        return filtered
    }, []);

  
    console.log('demo sidebar ',  sidebarData);
    return (
        <div className='sidebar-w'>
            <h4>Individual lessons</h4>            
            <SidebarItems
                xPos={null}
                yPos={null}
                sidebarData={sidebarData}
                moveItem={props.moveItem}
            />            
            <h4>Group lessons</h4>
            <div></div>
    </div>
    )
};

export default ScheduleSidebar;