import React, { Component } from "react";
import { DragLayer } from "react-dnd";

function collect(monitor) {
    return {
        item: monitor.getItem(),
        currentOffset: monitor.getSourceClientOffset(),
        isDragging: monitor.isDragging()
    };
}

function getItemTransform(props) {
    const { currentOffset } = props;
    if (!currentOffset) {
        return {
            display: "none"
        };
    }

    const { x, y } = currentOffset;
    const transform = `translate(${x}px, ${y}px) `;

    return {
        position: "fixed",
        pointerEvents: 'none',       
        height: "4em",
        width: props.widthElement - 20,
        zIndex: 10000,
        left: 0,
        top: 0,
        transform,
        WebkitTransform: transform,
        cursor: "move"
    };
}


class CustomDragLayer extends Component {
    constructor(props) {
        super(props);
        this.lastUpdate = +new Date();
        this.updateTimer = null;
    }


    shouldComponentUpdate(nextProps, nextState) {
        if (+new Date() - this.lastUpdate > 16) {
          this.lastUpdate = +new Date();
          clearTimeout(this.updateTimer);
          return true;
        } else {
          this.updateTimer = setTimeout(() => {
            this.forceUpdate();
          }, 100);
        }
        return false;
    }


    render() {
        const { item, isDragging } = this.props;
        if (!isDragging) {
            return null;
        }

        return (
            <div style={getItemTransform(this.props)} className="cell-box">
                <div
                    className={"cell-item " + item.sectionData.color}
                    // style={{ opacity: isDragging ? 0 : 1 }}
                >
                    <div>
                        <p>{"Drag " + item.sectionData.ID}</p>
                        <p>
                            {item.sectionData.level + "(" + item.sectionData.sixth + ")"}
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default DragLayer(collect)(CustomDragLayer);
