import React from 'react';
import { DropTarget } from 'react-dnd';
import SidebarItem from './SidebarItem.js';


const SubjectSectionTarget = {
    drop(props) {
        return props;
    },
};


const collect = (connect, monitor) => {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver() 
    }
};


const SidebarItems = (props) => {
    return props.connectDropTarget(
        <div className={'individual ' +  ( props.isOver ? 'is-over' : '' ) }> 
            {props.sidebarData.map( (item, index) => {
                return (
                    <SidebarItem
                        key={index}
                        moveItem={props.moveItem}
                        sectionData={item}
                    />
                )})
            }
        </div>
    )
};


export default DropTarget('subjectItem', SubjectSectionTarget, collect)(SidebarItems);


