import React, { Component } from 'react';
import { DragSource } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';


const subjectSource = {
    beginDrag(props, monitor, component) {
        return props;
    },

    endDrag(props, monitor, component) {
        if (!monitor.didDrop()) {
            return;
        }

        const item = monitor.getItem();
        const dropResult = monitor.getDropResult();

        props.moveItem(item.sectionData.ID, {
            day: dropResult.xPos,
            time: dropResult.yPos
        }, item.sectionData.numberLesson );
    }
}

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging(),
        connectDragPreview: connect.dragPreview()
    };
}


class SidebarItem extends Component {

    componentDidMount() {
        this.props.connectDragPreview(getEmptyImage(), {
			// IE fallback: specify that we'd rather screenshot the node
			// when it already knows it's being dragged so we can hide it with CSS.
			captureDraggingState: true,
		})
    }

    render() {
        // const { isDragging, moveItem } = this.props;
        const { connectDragSource, sectionData} = this.props;
        
        return connectDragSource(
            <div className={'sidebar-item '+ sectionData.color}>
                {sectionData.ID + ' - ' + sectionData.level + '('+ sectionData.sixth + ')'}
            </div>
        )
    }
} 

export default DragSource('subjectItem', subjectSource, collect)(SidebarItem);