import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import ScheduleColumn from './ScheduleColumn';
import GridDragLayer from './SidebarWeek/CustomDragLayer';


const nameWeekDays = ['Понеділок', 'Вівторок', 'Середа', 'Четверг', 'Пятниця', 'Субота'];


class ScheduleGrid extends Component {
    constructor(props) {
        super(props);
        this.weekDays = nameWeekDays;
        this.state = {
            widthElement: 0
        };
    }

    componentDidMount = () => {
        this.setState({
            widthElement: ReactDOM.findDOMNode(this.fisrtColumn).offsetWidth
        })
    };
    
    getFirstColumnRef = (node) => {this.fisrtColumn = node};


    printColumns = () => {
        return Array.from({length: this.props.columns}, (el, index) => (     
                <ScheduleColumn
                    { ...(index == 0 ? {ref: this.getFirstColumnRef} : {})}                    
                    itemsInColumn={this.props.timelineData}
                    moveItem={this.props.moveItem}
                    pupilsArray={this.props.pupilsArray}
                    key={index}
                    xPos={index + 1}
                    weekDay={this.weekDays[index]}
                />          
            )
        );
    }

    render() {       
        
        return (
            <Fragment>
                <GridDragLayer widthElement={this.state.widthElement}/> 
                <div className='table-w'> 
                    <ul>
                        {this.props.timelineData.map((el) => <li key={el.number}><span>{el.timeStart}</span></li> )}
                    </ul>
                    
                    {this.printColumns()}
                </div>
            </Fragment>
            
        )
    };
}


export default ScheduleGrid;
